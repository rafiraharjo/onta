<?php
class model_admin extends CI_Model
{

	public function get_all($table)
    {
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_where($table,$where)
    {
		$this->db->where($where);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_andWhere($table,$where1,$where2)
    {
        $this->db->where($where1);
        $this->db->where($where2);
        $query = $this->db->get($table);
        return $query->result_array();
    }

	public function get_where_numRows($table,$where)
    {
		$this->db->where($where);
        $query = $this->db->get($table);
        return $query->num_rows();
    }

    public function get_orWhere_numRows($table,$where1,$where2)
    {
		$this->db->where($where1);
		$this->db->or_where($where2);
        $query = $this->db->get($table);
        return $query->num_rows();
    }

    public function get_andWhere_numRows($table,$where1,$where2)
    {
        $this->db->where($where1);
        $this->db->where($where2);
        $query = $this->db->get($table);
        return $query->num_rows();
    }

	public function create($table,$data) {
		$query = $this->db->insert($table, $data);
		return $query;
	}

    function update($table,$data,$where)
    {
        $this->db->where($where);
        $query = $this->db->update($table, $data);
        return $query;
    }

    function update2($table,$data,$where1,$where2)
    {
        $this->db->where($where1);
        $this->db->where($where2);
        $query = $this->db->update($table, $data);
        return $query;
    }

    function delete($table,$where)
    {
        $query = $this->db->delete($table, $where);
        return $query;
    }

    public function getDosen()
	{
		$query = $this->db->select('fakultas.kode_fks, jurusan.nama_jur, dosen.id, dosen.nama_dsn, dosen.NIP, peminatan.nama_pmntn')
                 ->from('dosen')
                 ->join('peminatan', 'peminatan.id = dosen.id_pmntn', 'inner')
                 ->join('jurusan', 'jurusan.id = peminatan.id_jur', 'inner')
                 ->join('fakultas', 'fakultas.id = jurusan.id_fks', 'inner')
                 ->get();
        return $query->result_array();
	}

    public function getMahasiswa()
	{
		$query = $this->db->select('mahasiswa.id, mahasiswa.NIM, mahasiswa.nama_mhs, fakultas.kode_fks, jurusan.nama_jur, kelas.nama_kls')
                 ->from('mahasiswa')
                 ->join('kelas', 'kelas.id = mahasiswa.id_kls', 'inner')
                 ->join('jurusan', 'jurusan.id = kelas.id_jur', 'inner')
                 ->join('fakultas', 'fakultas.id = jurusan.id_fks', 'inner')
                 ->get();
        return $query->result_array();
    }
    
    public function getJurusan()
	{
		$query = $this->db->select('jurusan.id, jurusan.nama_jur, fakultas.kode_fks')
                 ->from('jurusan')
                 ->join('fakultas', 'fakultas.id = jurusan.id_fks', 'inner')
                 ->get();
        return $query->result_array();
    }
    
    public function get_where_jurusan($where)
	{
		$query = $this->db->select('jurusan.id as id_j, jurusan.nama_jur, jurusan.id_fks, fakultas.id as id_f, fakultas.kode_fks, fakultas.nama_fks')
                 ->from('jurusan')
                 ->join('fakultas', 'fakultas.id = jurusan.id_fks', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function getKelas()
	{
		$query = $this->db->select('kelas.id, kelas.nama_kls, jurusan.nama_jur')
                 ->from('kelas')
                 ->join('jurusan', 'jurusan.id = kelas.id_jur', 'inner')
                 ->get();
        return $query->result_array();
    }

    public function get_where_kelas($where)
	{
		$query = $this->db->select('kelas.id as id_k, kelas.nama_kls, kelas.id_jur, jurusan.id as id_j')
                 ->from('kelas')
                 ->join('jurusan', 'jurusan.id = kelas.id_jur', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function getPeminatan()
	{
		$query = $this->db->select('peminatan.id, peminatan.nama_pmntn, jurusan.nama_jur')
                 ->from('peminatan')
                 ->join('jurusan', 'jurusan.id = peminatan.id_jur', 'inner')
                 ->get();
        return $query->result_array();
    }

    public function get_where_peminatan($where)
	{
		$query = $this->db->select('peminatan.id as id_p, peminatan.nama_pmntn, peminatan.id_jur, jurusan.id as id_j')
                 ->from('peminatan')
                 ->join('jurusan', 'jurusan.id = peminatan.id_jur', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }
    
    public function join_where_sk($where1,$where2) {
        $query = $this->db->select()
                 ->from('bimbingan')
                 ->join('mahasiswa', 'mahasiswa.id = bimbingan.id_mahasiswa', 'inner')
                 ->join('kelas', 'kelas.id = mahasiswa.id_kls', 'inner')
                 ->join('jurusan', 'jurusan.id = kelas.id_jur', 'inner')
                 ->join('peminatan', 'peminatan.id = mahasiswa.id_pmntn', 'inner')
                 ->where($where1)
                 ->where($where2)
                 ->get();
        return $query->result_array();
    }
}