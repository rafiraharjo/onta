<?php
class model_dosen extends CI_Model
{

	public function get_all($table)
    {
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_where($table,$where)
    {
		$this->db->where($where);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_andWhere($table,$where1,$where2)
    {
        $this->db->where($where1);
        $this->db->where($where2);
        $query = $this->db->get($table);
        return $query->result_array();
    }

	public function get_where_numRows($table,$where)
    {
		$this->db->where($where);
        $query = $this->db->get($table);
        return $query->num_rows();
    }

    public function get_orWhere_numRows($table,$where1,$where2)
    {
		$this->db->where($where1);
		$this->db->or_where($where2);
        $query = $this->db->get($table);
        return $query->num_rows();
    }

    public function get_andWhere_numRows($table,$where1,$where2)
    {
        $this->db->where($where1);
        $this->db->where($where2);
        $query = $this->db->get($table);
        return $query->num_rows();
    }

	public function create($table,$data) {
		$query = $this->db->insert($table, $data);
		return $query;
	}

    function update($table,$data,$where)
    {
        $this->db->where($where);
        $query = $this->db->update($table, $data);
        return $query;
    }

    function update2($table,$data,$where1,$where2)
    {
        $this->db->where($where1);
        $this->db->where($where2);
        $query = $this->db->update($table, $data);
        return $query;
    }

    function delete($table,$where)
    {
        $query = $this->db->delete($table, $where);
        return $query;
    }

    public function join_where_profil($where) {
        $query = $this->db->select('fakultas.id as id_f, jurusan.id as id_j')
                 ->from('dosen')
                 ->join('peminatan', 'peminatan.id = dosen.id_pmntn', 'inner')
                 ->join('jurusan', 'jurusan.id = peminatan.id_jur', 'inner')
                 ->join('fakultas', 'fakultas.id = jurusan.id_fks', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_mhsBimbingan($where) {
        $query = $this->db->select('bimbingan.id_mahasiswa, mahasiswa.NIM, mahasiswa.nama_mhs, kelas.nama_kls, bimbingan.tema, bimbingan.status_bim')
                 ->from('bimbingan')
                 ->join('mahasiswa', 'mahasiswa.id = bimbingan.id_mahasiswa', 'inner')
                 ->join('kelas', 'kelas.id = mahasiswa.id_kls', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_detailMhs($where) {
        $query = $this->db->select('mahasiswa.id, mahasiswa.NIM, mahasiswa.nama_mhs, fakultas.nama_fks, jurusan.nama_jur, kelas.nama_kls, peminatan.nama_pmntn, bimbingan.tema, bimbingan.file, bimbingan.status_bim, bimbingan.catatan')
                 ->from('mahasiswa')
                 ->join('bimbingan', 'bimbingan.id_mahasiswa = mahasiswa.id', 'inner')
                 ->join('peminatan', 'peminatan.id= mahasiswa.id_pmntn', 'inner')
                 ->join('kelas', 'kelas.id = mahasiswa.id_kls', 'inner')
                 ->join('jurusan', 'jurusan.id = kelas.id_jur', 'inner')
                 ->join('fakultas', 'fakultas.id = jurusan.id_fks', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_progressMhs($where) {
        $query = $this->db->select('mahasiswa.NIM, mahasiswa.nama_mhs, kelas.nama_kls, progress.status, progress.id')
                 ->from('progress')
                 ->join('timeline', 'timeline.id = progress.id_timeline', 'inner')
                 ->join('mahasiswa', 'mahasiswa.id = progress.id_mahasiswa', 'inner')
                 ->join('kelas', 'kelas.id = mahasiswa.id_kls', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_detailProgressMhs($where) {
        $query = $this->db->select('mahasiswa.NIM, mahasiswa.nama_mhs, kelas.nama_kls, progress.status, progress.id, progress.file, bimbingan.tema')
                 ->from('progress')
                 ->join('mahasiswa', 'mahasiswa.id = progress.id_mahasiswa', 'inner')
                 ->join('kelas', 'kelas.id = mahasiswa.id_kls', 'inner')
                 ->join('bimbingan', 'bimbingan.id_mahasiswa = mahasiswa.id', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_timeline($where) {
        $query = $this->db->select('timeline.id as id_t')
                 ->from('progress')
                 ->join('timeline', 'timeline.id = progress.id_timeline', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_skBimbingan($where1,$where2) {
        $query = $this->db->select()
                 ->from('bimbingan')
                 ->join('mahasiswa', 'mahasiswa.id = bimbingan.id_mahasiswa', 'inner')
                 ->join('kelas', 'kelas.id = mahasiswa.id_kls', 'inner')
                 ->where($where1)
                 ->where($where2)
                 ->get();
        return $query->result_array();
    }

}