<?php
class model_mahasiswa extends CI_Model
{

	public function get_all($table)
    {
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_where($table,$where)
    {
		$this->db->where($where);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function get_andWhere($table,$where1,$where2)
    {
        $this->db->where($where1);
        $this->db->where($where2);
        $query = $this->db->get($table);
        return $query->result_array();
    }

	public function get_where_numRows($table,$where)
    {
		$this->db->where($where);
        $query = $this->db->get($table);
        return $query->num_rows();
    }

    public function get_orWhere_numRows($table,$where1,$where2)
    {
		$this->db->where($where1);
		$this->db->or_where($where2);
        $query = $this->db->get($table);
        return $query->num_rows();
    }

    public function get_andWhere_numRows($table,$where1,$where2)
    {
        $this->db->where($where1);
        $this->db->where($where2);
        $query = $this->db->get($table);
        return $query->num_rows();
    }

	public function create($table,$data) {
		$query = $this->db->insert($table, $data);
		return $query;
	}

    function update($table,$data,$where)
    {
        $this->db->where($where);
        $query = $this->db->update($table, $data);
        return $query;
    }

    public function join_where_profil($where) {
        $query = $this->db->select('fakultas.id as id_f, jurusan.id as id_j, kelas.id')
                 ->from('mahasiswa')
                 ->join('kelas', 'kelas.id = mahasiswa.id_kls', 'inner')
                 ->join('jurusan', 'jurusan.id = kelas.id_jur', 'inner')
                 ->join('fakultas', 'fakultas.id = jurusan.id_fks', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_request($where) {
        $query = $this->db->select('fakultas.nama_fks, jurusan.nama_jur, kelas.nama_kls, peminatan.nama_pmntn')
                 ->from('mahasiswa')
                 ->join('peminatan', 'peminatan.id = mahasiswa.id_pmntn', 'inner')
                 ->join('kelas', 'kelas.id = mahasiswa.id_kls', 'inner')
                 ->join('jurusan', 'jurusan.id = kelas.id_jur', 'inner')
                 ->join('fakultas', 'fakultas.id = jurusan.id_fks', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_request2($where) {
        $query = $this->db->select('dosen.id, dosen.nama_dsn, dosen.kuota')
                 ->from('mahasiswa')
                 ->join('peminatan', 'peminatan.id = mahasiswa.id_pmntn', 'inner')
                 ->join('dosen', 'dosen.id_pmntn = peminatan.id', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_request3($where) {
        $query = $this->db->select('dosen.nama_dsn, bimbingan.catatan, bimbingan.status_bim')
                 ->from('bimbingan')
                 ->join('dosen', 'dosen.id = bimbingan.id_dosen', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_tema($where) {
        $query = $this->db->select('dosen.nama_dsn, bimbingan.catatan, bimbingan.status_bim')
                 ->from('bimbingan')
                 ->join('dosen', 'dosen.id = bimbingan.id_dosen', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_timeline_numRows($where) {
        $query = $this->db->select()
                 ->from('timeline')
                 ->join('dosen', 'dosen.id = timeline.id_dosen', 'inner')
                 ->join('bimbingan', 'bimbingan.id_dosen = dosen.id', 'inner')
                 ->join('mahasiswa', 'mahasiswa.id = bimbingan.id_mahasiswa', 'inner')
                 ->where($where)
                 ->get();
        return $query->num_rows();
    }

    public function join_where_timeline($where) {
        $query = $this->db->select('timeline.id as id_t, timeline.progress, timeline.judul, timeline.due_date')
                 ->from('timeline')
                 ->join('dosen', 'dosen.id = timeline.id_dosen', 'inner')
                 ->join('bimbingan', 'bimbingan.id_dosen = dosen.id', 'inner')
                 ->join('mahasiswa', 'mahasiswa.id = bimbingan.id_mahasiswa', 'inner')
                 ->where($where)
                 ->get();
        return $query->result_array();
    }

    public function join_where_progress($where1,$where2) {
        $query = $this->db->select()
                 ->from('progress')
                 ->join('timeline', 'timeline.id = progress.id_timeline', 'inner')
                 ->where($where1)
                 ->where($where2)
                 ->get();
        return $query->result_array();
    }

}