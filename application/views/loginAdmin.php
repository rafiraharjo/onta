<!DOCTYPE html>
<html lang="en">
<head>
	<title>OnTA - Admin Login Page</title>
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" />
	<script src="<?php echo base_url(); ?>assets/jquery/jquery-3.4.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/login1.css" />
</head>
<body>
<div class="d-md-flex h-md-100 align-items-center">

	<div class="col-md-12 p-0 h-md-100">
	    <div class="align-items-center bg-admin h-100 justify-content-center">
	    	<div class="centered">
	        	
	        		<center>
	        		<h2 id="adm">Login Admin OnTA</h2><br>
	        		<form method="POST" action="<?php echo base_url(); ?>Login/loginAdmin">
	        		<table cellpadding="8">
	        			<tr>
	        				<td><label id="adm">Username</label></td>
	        				<td><input type="text" name="username" class="form-control"></td>
	        				<td rowspan="2" align="center"><input type="submit" name="submit" class="btn btn-success login-adm" value="Login"></td>
	        			</tr>
	        			<tr>
	        				<td><label id="adm">Password</label></td>
	        				<td><input type="password" name="password" class="form-control"></td>
	        			</tr>
	        		</table>
	        		</form>
	        		</center>
	        	
	        </div>
	    </div>
	</div>
    
</div>
</body>
</html>