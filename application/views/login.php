<!DOCTYPE html>
<html lang="en">
<head>
	<title>OnTA - Login Page</title>
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" />
	<script src="<?php echo base_url(); ?>assets/jquery/jquery-3.4.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/login.css" />
</head>
<body>
<div class="d-md-flex h-md-100 align-items-center">

	<!-- First Half -->

	<div class="col-md-6 p-0 h-md-100">
	    <div class="align-items-center bg-mahasiswa h-100 justify-content-center">
	    	<div class="row head bg-danger">
	    		<h3><b>OnTA “Online Tugas Akhir”</b></h3>
	    	</div>
	    	<div class="centered">
	        	<a href="<?php echo base_url(); ?>Login/mahasiswa"><button class="btn btn-danger awal"><h2><b>MAHASISWA</b></h2></button></a>
	        </div>
	    </div>
	</div>

	<!-- Second Half -->

	<div class="col-md-6 p-0 h-md-100">
	    <div class="align-items-center bg-dosen h-md-100 justify-content-center">
	        <div class="centered">
	        	<a href="<?php echo base_url(); ?>Login/dosen"><button class="btn btn-primary awal"><h2><b>DOSEN</b></h2></button></a>
	        </div>
	        <div class="row foot bg-primary">
	        	<div class="col-md-12">
	        		<h3><b>Powered By Telkom University</b></h3>
	        	</div>
	        </div>
	    </div>
	</div>
    
</div>
</body>
</html>