<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Hello, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				
				<div class="row nav1 active">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Dosen/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Dosen</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Dosen/mhsBimbingan">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Mahasiswa Bimbingan</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Dosen/timelineProgress">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Timeline Progress</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Dosen/skBimbingan">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">SK Bimbingan</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Dosen/skSiapSidang">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">SK Siap Sidang</p>
					</div>
				</div>
				</a>
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Dashboard</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<div class="box yellow">
					<br>
					<p id="box">Sisa Kuota Mahasiswa</p>
					<br>
					<h2><b>7/20</b></h2>
				</div>
				<div class="box red">
					<br>
					<p id="box">Jumlah Request</p>
					<br>
					<h2><b>2</b></h2>
				</div>
				<div class="box blue">
					<br>
					<p id="box">Jumlah Mahasiswa Bimbingan</p>
					<br>
					<h2><b>13/20</b></h2>
				</div>
			</div>
		</div>
		<br><br>
		<div class="row">
			<div class="col-md-12">
				<div class="box grey">
					<br>
					<p id="box">Jumlah Tema Disetujui</p>
					<br>
					<h2><b>9</b></h2>
				</div>
				<div class="box grey">
					<br>
					<p id="box">Jumlah Tema Diajukan</p>
					<br>
					<h2><b>3</b></h2>
				</div>
				<div class="box grey">
					<br>
					<p id="box">Belum Mengajukan Tema</p>
					<br>
					<h2><b>1</b></h2>
				</div>
			</div>
		</div>
	</div>
</div>