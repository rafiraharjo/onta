<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Hello, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				
				<div class="row nav1 active">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Admin/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Admin</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/akunDosen">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Akun Dosen</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/akunMahasiswa">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Akun Mahasiswa</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/fakultas">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Fakultas</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/jurusan">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Jurusan</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/kelas">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Kelas</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/peminatan">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Peminatan</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/skBimbingan">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">SK Bimbingan</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/skSiapSidang">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">SK Siap Sidang</p>
					</div>
				</div>
				</a>
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Dashboard</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<div class="box grey">
					<br>
					<p id="box">Jumlah Akun Dosen</p>
					<br>
					<h1><b>5</b></h1>
				</div>
				<div class="box grey">
					<br>
					<p id="box">Jumlah Akun Mahasiswa</p>
					<br>
					<h1><b>10</b></h1>
				</div>
				<div class="box grey">
					<br>
					<p id="box">Jumlah Fakultas</p>
					<br>
					<h1><b>3</b></h1>
				</div>
			</div>
		</div>
		<br><br>
		<div class="row">
			<div class="col-md-12">
				<div class="box grey">
					<br>
					<p id="box">Jumlah Jurusan</p>
					<br>
					<h1><b>3</b></h1>
				</div>
				<div class="box grey">
					<br>
					<p id="box">Jumlah Kelas</p>
					<br>
					<h1><b>3</b></h1>
				</div>
				<div class="box grey">
					<br>
					<p id="box">Jumlah Peminatan</p>
					<br>
					<h1><b>3</b></h1>
				</div>
			</div>
		</div>
		<br><br>
		<div class="row">
			<div class="col-md-12">
				
			</div>
		</div>
	</div>
</div>