<?php
foreach ($query as $row) {
	$id = $row['id'];
	$prog = $row['progress'];
	$judul = $row['judul'];
	$dueDate = $row['due_date'];
}
?>

<div class="row">
	<div class="col-md-3">
		<nav class="sidebar">
			<div class="row nav">
				<div class="col-md-12">
					<p id="navhead"><b>Hello, <?php echo $this->session->userdata('nama'); ?>!</b></p>
				</div>
			</div>
			<a href="<?php echo base_url(); ?>Dosen">
			<div class="row nav1">
				<div class="col-md-12">
					<p id="nav">Dashboard</p>
				</div>
			</div>
			</a>
			<a href="<?php echo base_url(); ?>Dosen/profil">
			<div class="row nav2">
				<div class="col-md-12">
					<p id="nav">Profil Dosen</p>
				</div>
			</div>
			</a>
			<a href="<?php echo base_url(); ?>Dosen/mhsBimbingan">
			<div class="row nav1">
				<div class="col-md-12">
					<p id="nav">Mahasiswa Bimbingan</p>
				</div>
			</div>
			</a>
				
			<div class="row nav2 active">
				<div class="col-md-12">
					<p id="nav">Timeline Progress</p>
				</div>
			</div>
			
			<a href="<?php echo base_url(); ?>Dosen/skBimbingan">
			<div class="row nav1">
				<div class="col-md-12">
					<p id="nav">SK Bimbingan</p>
				</div>
			</div>
			</a>
			<a href="<?php echo base_url(); ?>Dosen/skSiapSidang">
			<div class="row nav2">
				<div class="col-md-12">
					<p id="nav">SK Siap Sidang</p>
				</div>
			</div>
			</a>
		</nav>
	</div>

	<!-- Content -->
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Edit Progress</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<div class="panel">
					
					<form method="POST" action="<?php echo base_url(); ?>Dosen/updateProgress/<?php echo $id; ?>">
						<table cellpadding="8">
							<tr>
								<td><label>Progress ke -</label></td>
								<td width="100px"><input type="number" class="form-control" name="progress" width="50px" value="<?php echo $prog; ?>" disabled required></td>
								<td width="250px"></td>
							</tr>
							<tr>
								<td><label>Judul Progress</label></td>
								<td colspan="2"><input type="text" class="form-control" name="judul" value="<?php echo $judul; ?>" required></td>
							</tr>
							<tr>
								<td><label>Due Date</label></td>
								<td colspan="2"><input type="date" class="form-control" name="dueDate" value="<?php echo $dueDate; ?>" required></td>
							</tr>
							<tr>
								<td></td>
								<td colspan="2"><input type="submit" name="submit" id="submit" class="btn btn-success" value="Submit"> <button name="cancel" id="submit" class="btn btn-secondary" onclick="window.history.back();">Cancel</button></td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>