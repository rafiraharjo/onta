<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Hello, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Dosen">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Dosen/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Dosen</p>
					</div>
				</div>
				</a>
				
				<div class="row nav1 active">
					<div class="col-md-12">
						<p id="nav">Mahasiswa Bimbingan</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Dosen/timelineProgress">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Timeline Progress</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Dosen/skBimbingan">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">SK Bimbingan</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Dosen/skSiapSidang">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">SK Siap Sidang</p>
					</div>
				</div>
				</a>
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Mahasiswa Bimbingan</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-11">
				<table class="table">
					<thead class="thead-light">
						<tr>
							<th>NIM</th>
							<th>Nama</th>
							<th>Kelas</th>
							<th>Tema</th>
							<th>Status</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($query as $row) {
						?>
						<tr>
							<td><?php echo $row['NIM']?></td>
							<td><?php echo $row['nama_mhs']?></td>
							<td><?php echo $row['nama_kls']?></td>
							<td><?php echo $row['tema']?></td>
							<td><?php echo $row['status_bim']?></td>
							<td><a href="<?php echo base_url(); ?>Dosen/detailMahasiswa/<?php echo $row['id_mahasiswa']; ?>">Detail</a></td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>
</div>