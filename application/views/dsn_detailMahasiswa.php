<?php
foreach ($query as $row) {
	$id = $row['id'];
	$nim = $row['NIM'];
	$nama = $row['nama_mhs'];
	$fks = $row['nama_fks'];
	$jur = $row['nama_jur'];
	$kls = $row['nama_kls'];
	$pmntn = $row['nama_pmntn'];
	$tema = $row['tema'];
	$file = $row['file'];
	$status = $row['status_bim'];
	$catatan = $row['catatan'];
}
?>

<!-- Sidebar -->
<div class="row">
	<div class="col-md-3">
		<nav class="sidebar">
			<div class="row nav">
				<div class="col-md-12">
					<p id="navhead"><b>Hello, <?php echo $this->session->userdata('nama'); ?>!</b></p>
				</div>
			</div>
			<a href="<?php echo base_url(); ?>Dosen">
			<div class="row nav1">
				<div class="col-md-12">
					<p id="nav">Dashboard</p>
				</div>
			</div>
			</a>
			<a href="<?php echo base_url(); ?>Dosen/profil">
			<div class="row nav2">
				<div class="col-md-12">
					<p id="nav">Profil Dosen</p>
				</div>
			</div>
			</a>
			
			<div class="row nav1 active">
				<div class="col-md-12">
					<p id="nav">Mahasiswa Bimbingan</p>
				</div>
			</div>
			
			<a href="<?php echo base_url(); ?>Dosen/timelineProgress">
			<div class="row nav2">
				<div class="col-md-12">
					<p id="nav">Timeline Progress</p>
				</div>
			</div>
			</a>
			<a href="<?php echo base_url(); ?>Dosen/skBimbingan">
			<div class="row nav1">
				<div class="col-md-12">
					<p id="nav">SK Bimbingan</p>
				</div>
			</div>
			</a>
			<a href="<?php echo base_url(); ?>Dosen/skSiapSidang">
			<div class="row nav2">
				<div class="col-md-12">
					<p id="nav">SK Siap Sidang</p>
				</div>
			</div>
			</a>
		</nav>
	</div>

	<!-- Content -->
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Detail Mahasiswa Bimbingan</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<div class="panel">
					
						<table cellpadding="8">
							<tr>
								<td><label>NIM </label></td>
								<td width="450px"> <input type="number" class="form-control" name="nim" value="<?php echo $nim; ?>" required disabled></td>
							</tr>
							<tr>
								<td><label>Nama Lengkap </label></td>
								<td> <input type="text" class="form-control" name="nama" value="<?php echo $nama; ?>" required disabled></td>
							</tr>
							<tr>
								<td><label>Fakultas </label></td>
								<td> <input type="text" class="form-control" name="fakultas" value="<?php echo $fks; ?>" required disabled></td>
							</tr>
							<tr>
								<td><label>Jurusan </label></td>
								<td> <input type="text" class="form-control" name="jurusan" value="<?php echo $jur; ?>" required disabled></td>
							</tr>
							<tr>
								<td><label>Kelas </label></td>
								<td> <input type="text" class="form-control" name="kelas" value="<?php echo $kls; ?>" required disabled></td>
							</tr>
							<tr>
								<td><label>Peminatan </label></td>
								<td> <input type="text" class="form-control" name="peminatan" value="<?php echo $pmntn; ?>" required disabled></td>
							</tr>
							<tr>
								<td><label>Tema TA </label></td>
								<td> <input type="text" class="form-control" name="tema" value="<?php echo $tema; ?>" required disabled></td>
							</tr>
							<tr>
								<td><label>Abstraksi </label></td>
								<td> <button name="abstraksi" class="btn btn-secondary" id="submit" onclick="">Open File</button> <?php echo $file; ?></td>
							</tr>
							<?php
							if($status == "Approved"){
							?>
							<tr>
								<td><p style="color: green"><b>Status </b></p></td>
								<td> <p style="color: green"><b> : <?php echo $status; ?></b></p></td>
							</tr>
							<tr>
								<td></td>
								<td><a href="<?php echo base_url(); ?>Dosen/reviewMahasiswa/<?php echo $id; ?>"><button name="tolak" class="btn btn-danger" id="submit" onclick="">Tolak</button></a></td>
							</tr>
							<?php
							}elseif($status == "Finished"){
							?>
							<tr>
								<td><p style="color: green"><b>Status </b></p></td>
								<td> <p style="color: green"><b> : <?php echo $status; ?></b></p></td>
							</tr>
							<?php
							}elseif($status == "Rejected"){
							?>
							<tr>
								<td><p style="color: red"><b>Status </b></p></td>
								<td> <p style="color: red"><b> : <?php echo $status; ?></b></p></td>
							</tr>
							<tr>
								<td><label>Catatan </label></td>
								<td> <textarea name="catatan" class="form-control" disabled><?php echo $catatan; ?></textarea></td>
							</tr>
							<?php
							}else{
							?>
							<tr>
								<td></td>
								<td><a href="<?php echo base_url(); ?>Dosen/terimaMahasiswa/<?php echo $id; ?>"><button name="terima" class="btn btn-success" id="submit">Terima</button></a> <a href="<?php echo base_url(); ?>Dosen/reviewMahasiswa/<?php echo $id; ?>"><button name="tolak" class="btn btn-danger" id="submit">Tolak</button></a></td>
							</tr>
							<?php
							}
							?>
						</table>
				</div>
			</div>
		</div>
		<br><br><br>
	</div>
</div>