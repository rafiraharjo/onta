<?php
foreach ($query as $row){
	$id = $row['id'];
	$nim = $row['NIM'];
	$nama = $row['nama_mhs'];
	$kelas = $row['nama_kls'];
	$tema = $row['tema'];
	$file = $row['file'];
	$status = $row['status'];
}
?>

<div class="row">
	<div class="col-md-3">
		<nav class="sidebar">
			<div class="row nav">
				<div class="col-md-12">
					<p id="navhead"><b>Hello, <?php echo $this->session->userdata('nama'); ?>!</b></p>
				</div>
			</div>
			<a href="<?php echo base_url(); ?>Dosen">
			<div class="row nav1">
				<div class="col-md-12">
					<p id="nav">Dashboard</p>
				</div>
			</div>
			</a>
			<a href="<?php echo base_url(); ?>Dosen/profil">
			<div class="row nav2">
				<div class="col-md-12">
					<p id="nav">Profil Dosen</p>
				</div>
			</div>
			</a>
			<a href="<?php echo base_url(); ?>Dosen/mhsBimbingan">
			<div class="row nav1">
				<div class="col-md-12">
					<p id="nav">Mahasiswa Bimbingan</p>
				</div>
			</div>
			</a>
			
			<div class="row nav2 active">
				<div class="col-md-12">
					<p id="nav">Timeline Progress</p>
				</div>
			</div>
			
			<a href="<?php echo base_url(); ?>Dosen/skBimbingan">
			<div class="row nav1">
				<div class="col-md-12">
					<p id="nav">SK Bimbingan</p>
				</div>
			</div>
			</a>
			<a href="<?php echo base_url(); ?>Dosen/skSiapSidang">
			<div class="row nav2">
				<div class="col-md-12">
					<p id="nav">SK Siap Sidang</p>
				</div>
			</div>
			</a>
		</nav>
	</div>

	<!-- Content -->
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Detail Progress 1 Mahasiswa</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<div class="panel">
					<p id="title"><b>Form Penolakan Progress TA</b></p>
					<form method="POST" action="<?php echo base_url(); ?>Dosen/tolakProgressMhs/<?php echo $id; ?>">
						<table cellpadding="8">
							<tr>
								<td><label>NIM </label></td>
								<td width="450px"> <input type="number" class="form-control" name="nim" value="<?php echo $nim; ?>" required disabled></td>
							</tr>
							<tr>
								<td><label>Nama Lengkap </label></td>
								<td> <input type="text" class="form-control" name="nama" value="<?php echo $nama; ?>" required disabled></td>
							</tr>
							<tr>
								<td><label>Kelas </label></td>
								<td> <input type="text" class="form-control" name="kelas" value="<?php echo $kelas; ?>" required disabled></td>
							</tr>
							<tr>
								<td><label>Progress </label></td>
								<td> <button name="progress" class="btn btn-secondary">Open File</button> <?php echo $file; ?></td>
							</tr>
							<tr>
								<td><label>Catatan </label></td>
								<td> <textarea name="catatan" class="form-control"></textarea></td>
							</tr>
							<tr>
								<td></td>
								<td><input type="submit" name="submit" id="submit" class="btn btn-success" value="Submit"> <button name="cancel" id="submit" class="btn btn-secondary" onclick="window.history.back();">Cancel</button></td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>