<?php
foreach($sjur as $jur){
	$id_j = $jur['id_j'];
	$nama_j = $jur['nama_jur'];
	$id_f = $jur['id_fks'];
	$nama_f = $jur['nama_fks'];
}
?>
<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Hello, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Admin">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Admin</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/akunDosen">
				<div class="row nav1 active">
					<div class="col-md-12">
						<p id="nav">Akun Dosen</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/akunMahasiswa">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Akun Mahasiswa</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/fakultas">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Fakultas</p>
					</div>
				</div>
				</a>
				
				<div class="row nav2 active">
					<div class="col-md-12">
						<p id="nav">Jurusan</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Admin/kelas">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Kelas</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/peminatan">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Peminatan</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/skBimbingan">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">SK Bimbingan</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/skSiapSidang">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">SK Siap Sidang</p>
					</div>
				</div>
				</a>
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Edit Jurusan</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="panel">
				<div class="row">
					<div class="col-md-12">
						<p id="title"><b>Form Edit Jurusan</b></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<form method="POST" action="<?php echo base_url(); ?>Admin/updateJurusan/<?php echo $id_j;?>">
							<table cellpadding="8">
								<tr>
									<td><label>Jurusan </label></td>
									<td width="450px"> <input type="text" class="form-control" name="jurusan" value="<?php echo $nama_j; ?>" required></td>
									<!-- <input type="hidden" class="form-control" name="id_fks" value="<?php echo $id_f; ?>" required> -->
								</tr>
								<!-- <tr>
									<td><label>Ex-Fakultas </label></td>
									<td width="450px"> <input type="text" disabled="true" class="form-control" name="ex-fakultas" value="<?php echo $nama_f; ?>" required></td>
								</tr> -->
													<tr>
									<td><label>Fakultas </label></td>
									<td> <select class="form-control" name="fakultas" required>
										<option value="null" selected disabled>----- Pilih Fakultas -----</option>
										<?php foreach($fks as $fk){ ?>
										<option value="<?php echo $fk['id']; ?>" <?php if($id_f == $fk['id']){ echo "selected"; } ?>><?php echo $fk['nama_fks']; ?></option>					
										<?php } ?>
									</select></td>
								</tr>
								<tr>
									<td></td>
									<td><input type="submit" name="submit" id="submit" class="btn btn-success" value="Save"> <button name="cancel" id="submit" class="btn btn-secondary" onclick="window.history.back();">Cancel</button></td>
								</tr>
							</table>
						</form> 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>