<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Hello, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Dosen">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Dosen/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Dosen</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Dosen/mhsBimbingan">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Mahasiswa Bimbingan</p>
					</div>
				</div>
				</a>
				
				<div class="row nav2 active">
					<div class="col-md-12">
						<p id="nav">Timeline Progress</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Dosen/skBimbingan">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">SK Bimbingan</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Dosen/skSiapSidang">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">SK Siap Sidang</p>
					</div>
				</div>
				</a>
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-9">
				<h4 id="title">Timeline Progress</h4>
			</div>
			<div class="col-md-2">
				<a href="<?php echo base_url(); ?>Dosen/tambahProgress"><button name="addProgress" class="btn btn-primary">Tambah Progress</button></a>
			</div>
			<div class="col-md-1"></div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<?php
				if($con['condition'] == 1){
					foreach($query as $row){
				?>
				<div class="panel-timeline">
					<div class="row">
						<div class="col-md-10">
							<a href="<?php echo base_url(); ?>Dosen/progressMahasiswa/<?php echo $row['id']; ?>"><p id="title"><b>Progress <?php echo $row['progress']; ?></b> - <?php echo $row['judul']; ?></p></a>
							<p id="title">Due Date : <?php echo $row['due_date']; ?></p>
						</div>
						<div class="col-md-2">
							<a href="<?php echo base_url(); ?>Dosen/detailProgress/<?php echo $row['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Edit"><img class="timeline" src="<?php echo base_url(); ?>assets/icon/edit.png"></a>
							<a href="<?php echo base_url(); ?>Dosen/deleteProgress/<?php echo $row['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Delete"><img class="timeline" src="<?php echo base_url(); ?>assets/icon/delete.png"></a>
						</div>
					</div>
				</div>
				<?php
					}
				}else{
				?>
				<div class="panel-timeline">
					<div class="row">
						<div class="col-md-12">
							<p><b>Anda belum memiliki timeline.</b></p>
						</div>
					</div>
				</div>
				<?php
				}	
				?>
			</div>
		</div>
		<br><br><br>
	</div>
</div>