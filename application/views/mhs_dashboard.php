<?php
$bimb = "Not Available";

if($con['condition'] == 1){
	foreach ($query as $row) {
		$bimb = $row['status_bim'];
	}
}
?>

<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Hello, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				
				<div class="row nav1 active">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Mahasiswa/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Mahasiswa</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Mahasiswa/requestDosen">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Request Dosen Pembimbing</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Mahasiswa/timelineProgress">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Timeline Progress</p>
					</div>
				</div>
				</a>
			</nav>
		</div>	

	<!-- Content -->
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Dashboard</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<?php
				if($bimb == "Not Available"){
				?>
				<div class="box grey">
					<br>
					<p id="box">Status Bimbingan</p>
					<br>
					<h3><b>NOT AVAILABLE</b></h3>
				</div>
				<?php
				}elseif($bimb == "Submitted"){
				?>
				<div class="box yellow">
					<br>
					<p id="box">Status Bimbingan</p>
					<br>
					<h3><b>SUBMITTED</b></h3>
				</div>
				<?php
				}elseif($bimb == "Approved"){
				?>
				<div class="box green">
					<br>
					<p id="box">Status Bimbingan</p>
					<br>
					<h3><b>APPROVED</b></h3>
				</div>
				<?php
				}elseif($bimb == "Rejected"){
				?>
				<div class="box red">
					<br>
					<p id="box">Status Bimbingan</p>
					<br>
					<h3><b>REJECTED</b></h3>
				</div>
				<?php
				}elseif($bimb == "Finished"){
				?>
				<div class="box green">
					<br>
					<p id="box">Status Bimbingan</p>
					<br>
					<h3><b>FINISHED</b></h3>
				</div>
				<?php
				}
				?>
				
				<?php
				if($bimb == "Approved"){
				?>
				<div class="box yellow">
					<br>
					<p id="box">Status Tugas Akhir</p>
					<br>
					<h3><b>ON PROGRESS</b></h3>
				</div>
				<?php
				}elseif($bimb == "Finished"){
				?>
				<div class="box green">
					<br>
					<p id="box">Status Tugas Akhir</p>
					<br>
					<h3><b>SIAP SIDANG</b></h3>
				</div>
				<?php
				}else{
				?>
				<div class="box grey">
					<br>
					<p id="box">Status Tugas Akhir</p>
					<br>
					<h3><b>NOT AVAILABLE</b></h3>
				</div>
				<?php
				}
				?>
			</div>
		</div>
	</div>
</div>