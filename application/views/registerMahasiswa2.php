<!DOCTYPE html>
<html lang="en">
<head>
	<title>OnTA - Mahasiswa Login Page</title>
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" />
	<script src="<?php echo base_url(); ?>assets/jquery/jquery-3.4.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/login1.css" />
</head>
<body>
<div class="d-md-flex h-md-100 align-items-center">

	<div class="col-md-12 p-0 h-md-100">
	    <div class="align-items-center bg-mahasiswa h-100 justify-content-center">
	    	<div class="centered">
	        	<div class="panelReg">
	        		<center>
	        		<h2>Registrasi Mahasiswa</h2><br>
	        		<form method="POST" action="<?php echo base_url(); ?>Login/registerMahasiswa2/<?php echo $nim; ?>">
	        		<table cellpadding="8">
	        			<tr>
	        				<td><label>NIM</label></td>
	        				<td width="300px"><input type="text" name="nim" class="form-control" value="<?php echo $nim; ?>" required disabled></td>
	        			</tr>
	        			<tr>
	        				<td><label>Nama Lengkap</label></td>
	        				<td><input type="text" name="nama" class="form-control" required></td>
	        			</tr>
	        			<tr>
	        				<td><label>Jenis Kelamin</label></td>
	        				<td><input type="radio" name="jk" value="Laki-laki" required> <label> Laki-laki </label> / <input type="radio" name="jk"  value="Perempuan" required> <label> Perempuan </label></td>
	        			</tr>
	        			<tr>
	        				<td><label>Email</label></td>
	        				<td><input type="email" name="email" class="form-control" required></td>
	        			</tr>
	        			<tr>
	        				<td><label>Username</label></td>
	        				<td><input type="text" name="username" class="form-control" required></td>
	        			</tr>
	        			<tr>
	        				<td><label>Password</label></td>
	        				<td><input type="password" name="password" class="form-control" required></td>
	        			</tr>
	        			<tr>
	        				<td colspan="2" align="center"><br><input type="submit" name="submit" class="btn btn-success register" value="Submit"> <button name="cancel" class="btn btn-light register" onclick="window.history.back();">Cancel</button></td>
	        			</tr>
	        		</table>
	        		</form>
	        		</center>
	        	</div>
	        </div>
	    </div>
	</div>

</div>
</body>
</html>