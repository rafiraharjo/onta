<!DOCTYPE html>
<html lang="en">
<head>
	<title>OnTA - Mahasiswa Page</title>
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" />
	<script src="<?php echo base_url(); ?>assets/jquery/jquery-3.4.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/mahasiswa.css" />
</head>
<body>
<div class="container-fluid">

		<!-- Header -->
		<nav class="navbar fixed-top head">
			<div class="col-md-9">
				<h3><a href="<?php echo base_url(); ?>Mahasiswa" id="home">OnTA "Online Tugas Akhir"</a></h3>
			</div>
			<div class="col-md-2">
				<?php echo date("l , d - m - Y"); ?>
			</div>
			<div class="col-md-1">
				<a href="<?php echo base_url(); ?>Login/logout"><img src="<?php echo base_url(); ?>assets/icon/logout.png" id="logout" align="right" data-toggle="tooltip" data-placement="left" title="Log Out"></a>
			</div>
		</nav>
	