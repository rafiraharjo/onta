<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Hello, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Admin">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Admin</p>
					</div>
				</div>
				</a>
				
				<div class="row nav1 active">
					<div class="col-md-12">
						<p id="nav">Akun Dosen</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Admin/akunMahasiswa">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Akun Mahasiswa</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/fakultas">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Fakultas</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/jurusan">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Jurusan</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/kelas">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Kelas</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/peminatan">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Peminatan</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/skBimbingan">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">SK Bimbingan</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/skSiapSidang">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">SK Siap Sidang</p>
					</div>
				</div>
				</a>
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Tambah Dosen</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="panel">
				<div class="row">
					<div class="col-md-12">
						<p id="title"><b>Form Tambah Dosen Baru</b></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<form method="POST" action="<?php echo base_url(); ?>Admin/CreateDosen">
							<table cellpadding="8">
								<tr>
									<td><label>NIP </label></td>
									<td width="450px"> <input type="number" class="form-control" name="nip" required></td>
								</tr>
								<tr>
									<td><label>Nama Lengkap </label></td>
									<td> <input type="text" class="form-control" name="nama" required></td>
								</tr>
								<tr>
									<td><label>Jenis Kelamin </label></td>
									<td> <select class="form-control" name="jk" required>
										<option value="" selected disabled>----- Pilih Jenis Kelamin -----</option>
										<option value="Perempuan">Perempuan</option>	
										<option value="Laki-laki">Laki-laki</option>
									</select></td>
								</tr>
								<tr>
									<td><label>Fakultas </label></td>
									<td> <select class="form-control" name="fakultas" id="fakultas" data-child="jurusan" required>
										<option value="" selected disabled>----- Pilih Fakultas -----</option>
										<?php
										foreach ($fks as $row){
										?>
			                            <option value="<?php echo $row['id']; ?>" > <?php echo $row['nama_fks'] ?></option>
			                            <?php
			                            }
										?>
									</select></td>
								</tr>
								<tr>
									<td><label>Jurusan </label></td>
									<td> <select class="form-control" name="jurusan" id="jurusan" data-child="kelas" data-childd="peminatan" required>
										<option value="" selected disabled>----- Pilih Jurusan -----</option>
										<?php
										foreach ($jur as $row){
										?>
			                            <option data-group="<?php echo $row['id_fks']; ?>" value="<?php echo $row['id']; ?>" > <?php echo $row['nama_jur'] ?></option>
			                            <?php
			                            }
										?>
									</select></td>
								</tr>
								<tr>
									<td><label>Peminatan </label></td>
									<td> <select class="form-control" name="peminatan" id="peminatan" required>
										<option value="" selected disabled>----- Pilih Peminatan -----</option>
										<?php
										foreach ($pmnts as $row){
										?>
			                            <option data-group="<?php echo $row['id_jur']; ?>" value="<?php echo $row['id']; ?>" > <?php echo $row['nama_pmntn'] ?></option>
			                            <?php
			                            }
										?>
									</select></td>
								</tr>
								<tr>
									<td><label>Email </label></td>
									<td> <input type="email" class="form-control" name="email" required></td>
								</tr>
								<tr>
									<td><label>Username </label></td>
									<td> <input type="text" class="form-control" name="username" required></td>
								</tr>
								<tr>
									<td><label>Password </label></td>
									<td> <input type="password" class="form-control" name="password" required></td>
								</tr>
								<tr>
									<td><label>Kuota </label></td>
									<td> <input type="number" class="form-control" name="kuota" required></td>
								</tr>
								<tr>
									<td></td>
									<td><input type="submit" name="submit" id="submit" class="btn btn-success" value="Submit"> <input type="reset" name="cancel" id="reset" class="btn btn-secondary" value="Cancel"></td>
								</tr>
							</table>
						</form> 
					</div>
				</div>
			</div>
		</div>
		<br><br><br>
	</div>
</div>

<script type="text/javascript">
$("[data-child]").change(function() {
  //store reference to current select
  var me = $(this);

  //get selected group
  var group = me.find(":selected").val();

  //get the child select by it's ID
  var child = $("#" + me.attr("data-child"));

  //hide all child options except the ones for the current group, and get first item
  var newValue = child.find('option').hide().not('[data-group!="' + group + '"]').show().eq(0).val();
  child.trigger("change");

  //set default value
  child.val(newValue);
});

$("[data-childd]").change(function() {
  //store reference to current select
  var me = $(this);

  //get selected group
  var group = me.find(":selected").val();

  //get the child select by it's ID
  var child = $("#" + me.attr("data-childd"));

  //hide all child options except the ones for the current group, and get first item
  var newValue = child.find('option').hide().not('[data-group!="' + group + '"]').show().eq(0).val();
  child.trigger("change");

  //set default value
  child.val(newValue);
});
</script>