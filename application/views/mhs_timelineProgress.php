<?php
$bimb = "Not Available";

foreach ($query as $row) {
	$dosen = $row['nama_dsn'];
	$bimb = $row['status_bim'];
}
?>
<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Hello, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Mahasiswa">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Mahasiswa/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Mahasiswa</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Mahasiswa/requestDosen">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Request Dosen Pembimbing</p>
					</div>
				</div>
				</a>
				
				<div class="row nav2 active">
					<div class="col-md-12">
						<p id="nav">Timeline Progress</p>
					</div>
				</div>
				
			</nav>
		</div>	

	<!-- Content -->
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Timeline Progress</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<?php
				if($bimb == "Not Available" || $bimb == "Submitted" || $bimb == "Rejected"){
				?>
				<div class="panel-timeline">
					<div class="row">
						<div class="col-md-12">
							<p style="color: red;"><b>Silahkan selesaikan proses request dosen terlebih dahulu.</b></p>
						</div>
					</div>
				</div>
				<?php
				}elseif($bimb == "Approved"){
					if($con['condition'] == 1){
						foreach($query2 as $row){
				?>
				<a href="<?php echo base_url(); ?>Mahasiswa/detailProgress/<?php echo $row['id_t']; ?>">
				<div class="panel-timeline">
					<div class="row">
						<div class="col-md-12">
							<p id="title"><b>Progress <?php echo $row['progress']; ?></b> - <?php echo $row['judul']; ?></p>
							<p id="title">Due Date : <?php echo $row['due_date']; ?></p>
						</div>
					</div>
				</div>
				</a>
				<?php
						}
					}else{
				?>
				<div class="panel-timeline">
					<div class="row">
						<div class="col-md-12">
							<p><b>Progress belum dibuat oleh dosen pembimbing.</b></p>
						</div>
					</div>
				</div>
				<?php
					}
				}elseif($bimb == "Finished"){
				?>
				<div class="panel-timeline">
					<div class="row">
						<div class="col-md-12">
							<p style="color: green;"><b>Selamat! Anda telah dinyatakan siap sidang oleh dosen pembimbing.</b></p>
						</div>
					</div>
				</div>
				<?php
				}
				?>
			</div>
		</div>
	</div>
</div>