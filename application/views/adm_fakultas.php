<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Hello, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Admin">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Admin</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/akunDosen">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Akun Dosen</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/akunMahasiswa">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Akun Mahasiswa</p>
					</div>
				</div>
				</a>
				
				<div class="row nav1 active">
					<div class="col-md-12">
						<p id="nav">Fakultas</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Admin/jurusan">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Jurusan</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/kelas">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Kelas</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/peminatan">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Peminatan</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/skBimbingan">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">SK Bimbingan</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Admin/skSiapSidang">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">SK Siap Sidang</p>
					</div>
				</div>
				</a>
			</nav>
		</div>

	<!-- Content -->	
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-9">
				<h4 id="title">List Fakultas</h4>
			</div>
			<div class="col-md-2">
				<a href="<?php echo base_url(); ?>Admin/tambahFakultas"><button name="addFakultas" class="btn btn-primary">Tambah Fakultas</button></a>
			</div>
			<div class="col-md-1"></div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-11">
				<table class="table">
					<thead class="thead-light">
						<tr>
							<th>No</th>
							<th>Fakultas</th>
							<th>Kode Fakultas</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php $i = 1; 
						foreach($fks as $fk) { ?>
						<tr>
							<td><?php echo $i; ?></td>
							<td><?php echo $fk['nama_fks']; ?></td>
							<td><?php echo $fk['kode_fks']; ?></td>
							<td><a href="<?php echo base_url(); ?>Admin/editFakultas/<?php echo $fk['id']; ?>">Edit</a> / <a href="<?php echo base_url(); ?>Admin/hapusFakultas/<?php echo $fk['id']; ?>">Hapus</a></td>
						</tr>
						<?php  $i++;
					} ?>					
					</tbody>
				</table>
			</div>
			<div class="col-md-1"></div>
		</div>
		<br><br><br>
	</div>
</div>