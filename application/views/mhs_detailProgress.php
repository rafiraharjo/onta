<?php
foreach ($query as $row) {
	$id = $row['id'];
	$progress = $row['progress'];
	$judul = $row['judul'];
	$dueDate = $row['due_date'];

	if($con['condition'] == 1){
		$file = $row['file'];
		$catatan = $row['review'];
		$status = $row['status'];
	}
}
?>

<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Hello, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Mahasiswa">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Mahasiswa/profil">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Profil Mahasiswa</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Mahasiswa/requestDosen">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Request Dosen Pembimbing</p>
					</div>
				</div>
				</a>
				
				<div class="row nav2 active">
					<div class="col-md-12">
						<p id="nav">Timeline Progress</p>
					</div>
				</div>
				
			</nav>
		</div>	

	<!-- Content -->
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Submit Progress <?php echo $progress; ?></h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<div class="panel">
					<p id="title"><b>Progress <?php echo $progress; ?></b> - <?php echo $judul; ?></p>
					<p id="title">Due Date : <?php echo $dueDate; ?></p>
					<br>
					<form method="POST" action="<?php echo base_url(); ?>Mahasiswa/submitProgress/<?php echo $id; ?>">
						<table cellpadding="8">
							<?php
							if($con['condition'] == 1){
								if($status == "Submitted"){
							?>
							<tr>
								<td><p style="color: orange;"><b>Status Progress</b></p></td>
								<td><p style="color: orange;"><b> : <?php echo $status; ?></b></p></td>
							</tr>
							<tr>
								<td><label>Progress</label></td>
								<td><input type="file" name="progress" required></td>
							</tr>
							<tr>
								<td></td>
								<td><input type="submit" name="submit" id="submit" class="btn btn-success" value="Submit"> <button name="cancel" id="cancel" class="btn btn-secondary" onclick="window.history.back();">Cancel</button></td>
							</tr>
							<?php
								}elseif($status == "Approved"){
							?>
							<tr>
								<td><p style="color: green;"><b>Status Progress</b></p></td>
								<td><p style="color: green;"><b> : <?php echo $status; ?></b></p></td>
							</tr>
							<?php
								}elseif ($status == "Rejected") {
							?>
							<tr>
								<td><p style="color: red;"><b>Status Progress</b></p></td>
								<td><p style="color: red;"><b> : <?php echo $status; ?></b></p></td>
							</tr>
							<tr>
								<td><label>Catatan</label></td>
								<td><textarea name="catatan" disabled><?php echo $catatan; ?></textarea></td>
							</tr>
							<tr>
								<td><label>Progress</label></td>
								<td><input type="file" name="progress" required></td>
							</tr>
							<tr>
								<td></td>
								<td><input type="submit" name="submit" id="submit" class="btn btn-success" value="Submit"> <button name="cancel" id="cancel" class="btn btn-secondary" onclick="window.history.back();">Cancel</button></td>
							</tr>
							<?php
								}
							}else{
							?>
							<tr>
								<td><label>Progress</label></td>
								<td><input type="file" name="progress" required></td>
							</tr>
							<tr>
								<td></td>
								<td><input type="submit" name="submit" id="submit" class="btn btn-success" value="Submit"> <button name="cancel" id="cancel" class="btn btn-secondary" onclick="window.history.back();">Cancel</button></td>
							</tr>
							<?php
							}
							?>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>