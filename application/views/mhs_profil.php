<?php
foreach ($query as $row) {
	$nim = $row['NIM'];
	$nama = $row['nama_mhs'];
	$jk = $row['jk'];
	$email = $row['email'];
	$username = $row['username'];
	$id_kel = $row['id_kls'];
	$id_pmntn = $row['id_pmntn'];
}

$jur = '';
$fks = '';

foreach ($query6 as $row) {
	$jur = $row['id_j'];
	$fks = $row['id_f'];
}
?>
<!-- Sidebar -->
<div class="row">
		<div class="col-md-3">
			<nav class="sidebar">
				<div class="row nav">
					<div class="col-md-12">
						<p id="navhead"><b>Hello, <?php echo $this->session->userdata('nama'); ?>!</b></p>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>Mahasiswa">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Dashboard</p>
					</div>
				</div>
				</a>
				
				<div class="row nav2 active">
					<div class="col-md-12">
						<p id="nav">Profil Mahasiswa</p>
					</div>
				</div>
				
				<a href="<?php echo base_url(); ?>Mahasiswa/requestDosen">
				<div class="row nav1">
					<div class="col-md-12">
						<p id="nav">Request Dosen Pembimbing</p>
					</div>
				</div>
				</a>
				<a href="<?php echo base_url(); ?>Mahasiswa/timelineProgress">
				<div class="row nav2">
					<div class="col-md-12">
						<p id="nav">Timeline Progress</p>
					</div>
				</div>
				</a>
			</nav>
		</div>	

	<!-- Content -->
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Profil Mahasiswa</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="panel">
				<div class="row">
					<div class="col-md-4">
						<img class="profile" src="<?php echo base_url(); ?>assets/icon/profile.png">
					</div>
					<div class="col-md-8">
						<form method="POST" action="<?php echo base_url(); ?>Mahasiswa/updateProfil">
							<table cellpadding="8">
								<tr>
									<td><label>NIM </label></td>
									<td width="450px"> <input type="number" class="form-control" name="nim" value="<?php echo $nim; ?>" required></td>
								</tr>
								<tr>
									<td><label>Nama Lengkap </label></td>
									<td> <input type="text" class="form-control" name="nama" value="<?php echo $nama; ?>" required></td>
								</tr>
								<tr>
									<td><label>Jenis Kelamin </label></td>
									<td><input type="radio" name="jk" value="Laki-laki" <?php if($jk == "Laki-laki"){ echo "checked"; } ?> required> Laki-laki / <input type="radio" name="jk" value="Perempuan" <?php if($jk == "Perempuan"){ echo "checked"; } ?> required> Perempuan </td>
								</tr>
								<tr>
									<td><label>Fakultas </label></td>
									<td> <select class="form-control" name="fakultas" id="fakultas" data-child="jurusan" required>
										<option value="" selected disabled>----- Pilih Fakultas -----</option>
										<?php
										foreach ($query2 as $row){
										?>
			                            <option value="<?php echo $row['id']; ?>" <?php if($fks == $row['id']){ echo "selected"; } ?> > <?php echo $row['nama_fks'] ?></option>
			                            <?php
			                            }
										?>
									</select></td>
								</tr>
								<tr>
									<td><label>Jurusan </label></td>
									<td> <select class="form-control" name="jurusan" id="jurusan" data-child="kelas" data-childd="peminatan" required>
										<option value="" selected disabled>----- Pilih Jurusan -----</option>
										<?php
										foreach ($query3 as $row){
										?>
			                            <option data-group="<?php echo $row['id_fks']; ?>" value="<?php echo $row['id']; ?>" <?php if($jur == $row['id']){ echo "selected"; } ?> > <?php echo $row['nama_jur'] ?></option>
			                            <?php
			                            }
										?>
									</select></td>
								</tr>
								<tr>
									<td><label>Kelas </label></td>
									<td> <select class="form-control" name="kelas" id="kelas" required>
										<option value="" selected disabled>----- Pilih Kelas -----</option>
										<?php
										foreach ($query4 as $row){
			                            ?>
			                            <option data-group="<?php echo $row['id_jur']; ?>" value="<?php echo $row['id']; ?>" <?php if($id_kel == $row['id']){ echo "selected"; } ?> > <?php echo $row['nama_kls'] ?></option>
			                            <?php
			                            }
										?>
									</select></td>
								</tr>
								<tr>
									<td><label>Peminatan </label></td>
									<td> <select class="form-control" name="peminatan" id="peminatan" required>
										<option value="" selected disabled>----- Pilih Peminatan -----</option>
										<?php
										foreach ($query5 as $row){
										?>
			                            <option data-group="<?php echo $row['id_jur']; ?>" value="<?php echo $row['id']; ?>" <?php if($id_pmntn == $row['id']){ echo "selected"; } ?> > <?php echo $row['nama_pmntn'] ?></option>
			                            <?php
			                            }
										?>
									</select></td>
								</tr>
								<tr>
									<td><label>Email </label></td>
									<td> <input type="email" class="form-control" name="email" value="<?php echo $email; ?>" required></td>
								</tr>
								<tr>
									<td><label>Username </label></td>
									<td> <input type="text" class="form-control" name="username" value="<?php echo $username; ?>" required></td>
								</tr>
								<tr>
									<td><br><label>Password Baru </label></td>
									<td> <br><input type="password" class="form-control" name="password"></td>
								</tr>
								<tr>
									<td></td>
									<td><input type="submit" name="submit" id="submit" class="btn btn-success" value="Save"></td>
								</tr>
							</table>
						</form> 
					</div>
				</div>
			</div>
		</div>
		<br><br><br>
	</div>
</div>

<script type="text/javascript">
$("[data-child]").change(function() {
  //store reference to current select
  var me = $(this);

  //get selected group
  var group = me.find(":selected").val();

  //get the child select by it's ID
  var child = $("#" + me.attr("data-child"));

  //hide all child options except the ones for the current group, and get first item
  var newValue = child.find('option').hide().not('[data-group!="' + group + '"]').show().eq(0).val();
  child.trigger("change");

  //set default value
  child.val(newValue);
});

$("[data-childd]").change(function() {
  //store reference to current select
  var me = $(this);

  //get selected group
  var group = me.find(":selected").val();

  //get the child select by it's ID
  var child = $("#" + me.attr("data-childd"));

  //hide all child options except the ones for the current group, and get first item
  var newValue = child.find('option').hide().not('[data-group!="' + group + '"]').show().eq(0).val();
  child.trigger("change");

  //set default value
  child.val(newValue);
});
</script>