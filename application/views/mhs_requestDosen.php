<?php
$id_kel = '';
$id_pmntn = '';

foreach ($query as $row) {
	$nim = $row['NIM'];
	$nama = $row['nama_mhs'];
	$id_kel = $row['id_kls'];
	$id_pmntn = $row['id_pmntn'];
}

foreach ($query2 as $row) {
	$fks = $row['nama_fks'];
	$jur = $row['nama_jur'];
	$kls = $row['nama_kls'];
	$pmn = $row['nama_pmntn'];
}

?>
<!-- Sidebar -->
<div class="row">
	<div class="col-md-3">
		<nav class="sidebar">
			<div class="row nav">
				<div class="col-md-12">
					<p id="navhead"><b>Hello, <?php echo $this->session->userdata('nama'); ?>!</b></p>
				</div>
			</div>
			<a href="<?php echo base_url(); ?>Mahasiswa">
			<div class="row nav1">
				<div class="col-md-12">
					<p id="nav">Dashboard</p>
				</div>
			</div>
			</a>
			<a href="<?php echo base_url(); ?>Mahasiswa/profil">
			<div class="row nav2">
				<div class="col-md-12">
					<p id="nav">Profil Mahasiswa</p>
				</div>
			</div>
			</a>
			
			<div class="row nav1 active">
				<div class="col-md-12">
					<p id="nav">Request Dosen Pembimbing</p>
				</div>
			</div>
			
			<a href="<?php echo base_url(); ?>Mahasiswa/timelineProgress">
			<div class="row nav2">
				<div class="col-md-12">
					<p id="nav">Timeline Progress</p>
				</div>
			</div>
			</a>
		</nav>
	</div>	

	<!-- Content -->
	<div class="col-md-9 content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<h4 id="title">Request Dosen Pembimbing</h4>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="panel">
				<?php
				if($id_kel == 0 || $id_pmntn == 0){
				?>
				<div class="row">
					<div class="col-md-12">
						<p style="color: red;"><b>Lengkapi data profil Anda terlebih dahulu!</b></p>
					</div>
				</div>
				<?php
				}elseif($con['condition'] == 1 && $con['status_bim'] == "Submitted"){
				?>
				<div class="row">
					<div class="col-md-12">
						<p style="color: orange;"><b>Anda sudah melakukan request dosen.</b></p>
						<p><b>Status : <?php echo $con['status_bim']; ?></b></p>
						<br>
						<p><b>Dosen Pembimbing : <?php echo $con['nama_dsn']; ?></b></p>
					</div>
				</div>
				<?php
				}elseif($con['condition'] == 1 && ($con['status_bim'] == "Approved" || $con['status_bim'] == "Finished")){
				?>
				<div class="row">
					<div class="col-md-12">
						<p style="color: green;"><b>Anda telah menyelesaikan proses request dosen.</b></p>
						<p><b>Status : <?php echo $con['status_bim']; ?></b></p>
						<br>
						<p><b>Dosen Pembimbing : <?php echo $con['nama_dsn']; ?></b></p>
					</div>
				</div>
				<?php
				}else{
				?>
				<div class="row">
					<div class="col-md-12">
						<p id="title"><b>Form Request Dosen Pembimbing</b></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<form method="POST" action="<?php echo base_url(); ?>Mahasiswa/proses_requestDosen">
							<table cellpadding="8">
								<tr>
									<td><label>NIM </label></td>
									<td width="450px"> <input type="text" class="form-control" name="nim" value="<?php echo $nim; ?>" required disabled></td>
								</tr>
								<tr>
									<td><label>Nama Lengkap </label></td>
									<td> <input type="text" class="form-control" name="nama" value="<?php echo $nama; ?>" required disabled></td>
								</tr>
								<tr>
									<td><label>Fakultas </label></td>
									<td> <input type="text" class="form-control" name="fakultas" value="<?php echo $fks; ?>" required disabled></td>
								</tr>
								<tr>
									<td><label>Jurusan </label></td>
									<td> <input type="text" class="form-control" name="jurusan" value="<?php echo $jur; ?>" required disabled></td>
								</tr>
								<tr>
									<td><label>Kelas </label></td>
									<td> <input type="text" class="form-control" name="nim" value="<?php echo $kls; ?>" required disabled></td>
								</tr>
								<tr>
									<td><label>Peminatan </label></td>
									<td> <input type="text" class="form-control" name="nama" value="<?php echo $pmn; ?>" required disabled></td>
								</tr>
								<?php
								if($con['condition'] == 1){
									if($con['status_bim'] == "Rejected"){
								?>
								<tr>
									<td><br><p style="color: red;"><b>Request ditolak oleh </b></p></td>
									<td><br><p> : <?php echo $con['nama_dsn']; ?></p></td>
								</tr>
								<tr>
									<td><label>Catatan </label></td>
									<td> <textarea name="catatan" disabled><?php echo $con['catatan']; ?></textarea></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="panel">
					<div class="row">
						<div class="col-md-12">
							<p id="title"><b>Request Dosen Pembimbing Baru</b></p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<table cellpadding="8">
								<?php
									}
								}
								?>
								<tr>
									<td><label>Dosen Pembimbing </label></td>
									<td> <select class="form-control" name="dosen" required>
										<option value="" selected disabled>----- Pilih Dosen -----</option>
										<?php
										foreach ($query3 as $row){
										?>
			                            <option value="<?php echo $row['id']; ?>" <?php if($row['kuota'] == 0){ echo "disabled"; } ?> > <?php echo $row['nama_dsn']; ?> <?php if($row['kuota'] == 0){ echo "(kuota penuh)"; } ?></option>
			                            <?php
			                            }
										?>
									</select>
									</td>
								</tr>
								<tr>
									<td><label>Tema TA </label></td>
									<td> <input type="text" class="form-control" name="tema" required></td>
								</tr>
								<tr>
									<td><label>File Abstraksi </label></td>
									<td> <input type="file" name="abstraksi" required></td>
								</tr>
								<tr>
									<td></td>
									<td align="right"><input type="submit" name="submit" id="submit" class="btn btn-success" value="Submit"> <input type="reset" name="reset" id="reset" class="btn btn-secondary" value="Cancel"></td>
								</tr>
							</table>
						</form>
					</div>
				</div>
				<?php
				}
				?>
			</div>
		</div>
		<br><br><br>
	</div>
</div>