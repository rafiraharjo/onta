<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen extends CI_Controller {

	public function __construct(){
		parent:: __construct();
		
		$this->load->model('model_dosen');
		if($this->session->userdata('level') == '' || $this->session->userdata('level') != 'dosen'){
			redirect("Login/");
		}
	}

	public function index()
	{
		$this->load->view('dsn_header');
		$this->load->view('dsn_dashboard');
		$this->load->view('dsn_footer');
	}

	public function profil()
	{
		$table = "dosen";
		$where = array (
			'id' => $this->session->userdata('id'));
		$data['query'] = $this->model_dosen->get_where($table,$where);

		$table2 = "fakultas";
		$data['query2'] = $this->model_dosen->get_all($table2);

		$table3 = "jurusan";
		$data['query3'] = $this->model_dosen->get_all($table3);

		$table4 = "peminatan";
		$data['query4'] = $this->model_dosen->get_all($table4);

		$where2 = array (
			'dosen.id' => $this->session->userdata('id'));
		$data['query5'] = $this->model_dosen->join_where_profil($where2); 

		$this->load->view('dsn_header');
		$this->load->view('dsn_profil',$data);
		$this->load->view('dsn_footer');
	}

	public function updateProfil(){
		if($this->input->post('submit')){
			$table = "dosen";
			$where = array (
				'id' => $this->session->userdata('id'));
			if($this->input->post('password') != ''){
				$data = array (
					'NIP' => $this->input->post('nip'),
			        'nama_dsn' => $this->input->post('nama'),
			        'jk' => $this->input->post('jk'),
			        'email' => $this->input->post('email'),
			        'username' => $this->input->post('username'),
			        'password' => $this->input->post('password'),
			        'id_pmntn' => $this->input->post('peminatan'),
			        'kuota' => $this->input->post('kuota'));
			}else{
				$data = array (
					'NIP' => $this->input->post('nip'),
			        'nama_dsn' => $this->input->post('nama'),
			        'jk' => $this->input->post('jk'),
			        'email' => $this->input->post('email'),
			        'username' => $this->input->post('username'),
			        'id_pmntn' => $this->input->post('peminatan'),
			        'kuota' => $this->input->post('kuota'));
			}
		    $query = $this->model_dosen->update($table,$data,$where);

		    if(isset($query)){
		        echo "<script>alert('Profil berhasil disimpan.');window.location.href='profil'</script>";
		    }else{
		        echo "<script>alert('Profil gagal disimpan!');window.location.href='profil'</script>";
		    }
		}
	}

	public function mhsBimbingan()
	{
		$where = array (
			'id_dosen' => $this->session->userdata('id'));
		$data['query'] = $this->model_dosen->join_where_mhsBimbingan($where);

		$this->load->view('dsn_header');
		$this->load->view('dsn_mhsBimbingan',$data);
		$this->load->view('dsn_footer');
	}

	public function detailMahasiswa($id)
	{
		$where = array (
			'mahasiswa.id' => $id);
		$data['query'] = $this->model_dosen->join_where_detailMhs($where);

		$this->load->view('dsn_header');
		$this->load->view('dsn_detailMahasiswa',$data);
		$this->load->view('dsn_footer');
	}

	public function reviewMahasiswa($id)
	{
		$where = array (
			'bimbingan.id_mahasiswa' => $id);
		$data['query'] = $this->model_dosen->join_where_detailMhs($where);

		$this->load->view('dsn_header');
		$this->load->view('dsn_tolakMahasiswa',$data);
		$this->load->view('dsn_footer');
	}

	public function terimaMahasiswa($id)
	{
		$table = "bimbingan";
		$where = array (
			'id_mahasiswa' => $id);
		$data = array (
			'status_bim' => "Approved");
		$query = $this->model_dosen->update($table,$data,$where);

		if(isset($query)){
		    echo "<script>alert('Request mahasiswa telah disetujui.');window.location.href='../mhsBimbingan'</script>";
		}else{
		    echo "<script>alert('Update gagal!');window.location.href='../detailMahasiswa/".$nim."'</script>";
		}
	}

	public function tolakMahasiswa($id)
	{
		$table1 = "bimbingan";
		$where1 = array (
			'id_mahasiswa' => $id);
		$data1 = array (
			'catatan' => $this->input->post('catatan'),
			'status_bim' => "Rejected");
		$query1 = $this->model_dosen->update($table1,$data1,$where1);

		$table2 = "dosen";
		$where2 = array (
			'id' => $this->session->userdata('id'));
		$query2 = $this->model_dosen->get_where($table2,$where2);
		$kuota = $query2[0]['kuota'];

		$data2 = array (
			'kuota' => $kuota+1);
		$query3 = $this->model_dosen->update($table2,$data2,$where2);

		if(isset($query1) && isset($query3)){
		    echo "<script>alert('Request mahasiswa telah ditolak.');window.location.href='../mhsBimbingan'</script>";
		}else{
		    echo "<script>alert('Update gagal!');window.location.href='../detailMahasiswa/".$nim."'</script>";
		}
	}

	public function timelineProgress()
	{
		$table = "timeline";
		$where = array (
			'id_dosen' => $this->session->userdata('id'));
		$query['row'] = $this->model_dosen->get_where_numRows($table,$where);

		if($query['row'] > 0){
			$data['query'] = $this->model_dosen->get_where($table,$where);
			$data['con'] = array (
				'condition' => 1);
		}else{
			$data['con'] = array (
				'condition' => 0);
		}

		$this->load->view('dsn_header');
		$this->load->view('dsn_timelineProgress',$data);
		$this->load->view('dsn_footer');
	}

	public function tambahProgress()
	{
		$this->load->view('dsn_header');
		$this->load->view('dsn_tambahProgress');
		$this->load->view('dsn_footer');
	}

	public function addProgress()
	{
		if($this->input->post('submit')){
			$prog = $this->input->post('progress');
			$table = "timeline";
			$where1 = array (
				'id_dosen' => $this->session->userdata('id'));
			$where2 = array (
				'progress' => $prog);
			
			$row = $this->model_dosen->get_andWhere_numRows($table,$where1,$where2);

			if($row == 0){
				$data = array (
					'id_dosen' => $this->session->userdata('id'),
					'progress' => $this->input->post('progress'),
					'judul' => $this->input->post('judul'),
					'due_date' => $this->input->post('dueDate'));
				$query = $this->model_dosen->create($table,$data);

				if(isset($query)){
					echo "<script>alert('Progress berhasil ditambahkan.');window.location.href='timelineProgress'</script>";
				}else{
					echo "<script>alert('Progress gagal ditambahkan!');window.location.href='tambahProgress'</script>";
				}
			}else{
				echo "<script>alert('Progress gagal ditambahkan. Progress ".$prog." sudah ada! ');window.location.href='tambahProgress'</script>";
			}

		}
	}

	public function detailProgress($id)
	{
		$table = "timeline";
		$where = array (
			'id' => $id);
		$data['query'] = $this->model_dosen->get_where($table,$where);

		$this->load->view('dsn_header');
		$this->load->view('dsn_detailProgress', $data);
		$this->load->view('dsn_footer');
	}

	public function updateProgress($id){
		if($this->input->post('submit')){
			$table = "timeline";
			$where = array (
				'id' => $id);
			$data = array (
				'judul' => $this->input->post('judul'),
			    'due_date' => $this->input->post('dueDate'));
			$query = $this->model_dosen->update($table,$data,$where);

			if(isset($query)){
		    	echo "<script>alert('Progress berhasil diupdate.');window.location.href='../timelineProgress'</script>";
			}else{
		    	echo "<script>alert('Progress gagal diupdate!');window.location.href='../timelineProgress'</script>";
			}
		}
	}

	public function deleteProgress($id)
	{
		$table = "timeline";
		$where = array (
			'id' => $id);
		$query = $this->model_dosen->delete($table,$where);

		if(isset($query)){
		    echo "<script>alert('Progress berhasil dihapus.');window.location.href='../timelineProgress'</script>";
		}else{
		    echo "<script>alert('Progress gagal dihapus!');window.location.href='../timelineProgress'</script>";
		}
	}

	public function progressMahasiswa($id)
	{
		$where = array (
			'progress.id_timeline' => $id);
		$table2 = "timeline";
		$where2 = array (
			'id' => $id);
		$tl = $this->model_dosen->get_where($table2,$where2);
		$data['query'] = $this->model_dosen->join_where_progressMhs($where);
		$data['con'] = array (
			'progress' => $tl[0]['progress']);
			
		$this->load->view('dsn_header');
		$this->load->view('dsn_progressMahasiswa',$data);
		$this->load->view('dsn_footer');
	}

	public function detailProgressMhs($id)
	{
		$where = array (
			'progress.id' => $id);
		$data['query'] = $this->model_dosen->join_where_detailProgressMhs($where);

		$this->load->view('dsn_header');
		$this->load->view('dsn_detailProgressMhs',$data);
		$this->load->view('dsn_footer');
	}

	public function terimaProgressMhs($id)
	{
		$table = "progress";
		$where = array (
			'progress.id' => $id);
		$data = array (
			'status' => "Approved");
		$query = $this->model_dosen->update($table,$data,$where);

		$tl = $this->model_dosen->join_where_timeline($where);
		$id_tl = $tl[0]['id_t'];

		if(isset($query)){
			echo "<script>alert('Progress mahasiswa berhasil disetujui.');window.location.href='../progressMahasiswa/".$id_tl."'</script>";
		}else{
			echo "<script>alert('Progress mahasiswa gagal disetujui!');window.location.href='../progressMahasiswa/".$id_tl."'</script>";
		}
	}

	public function tolakProgress($id)
	{
		$where = array (
			'progress.id' => $id);
		$data['query'] = $this->model_dosen->join_where_detailProgressMhs($where);

		$this->load->view('dsn_header');
		$this->load->view('dsn_tolakProgress',$data);
		$this->load->view('dsn_footer');
	}

	public function tolakProgressMhs($id)
	{
		if($this->input->post('submit')){
			$table = "progress";
			$where = array (
				'progress.id' => $id);
			$data = array (
				'review' => $this->input->post('catatan'),
				'status' => "Rejected");
			$query = $this->model_dosen->update($table,$data,$where);
			$tl = $this->model_dosen->join_where_timeline($where);
			$id_tl = $tl[0]['id_t'];

			if(isset($query)){
				echo "<script>alert('Progress mahasiswa berhasil direview.');window.location.href='../progressMahasiswa/".$id_tl."'</script>";
			}else{
				echo "<script>alert('Progress mahasiswa gagal direview!');window.location.href='../progressMahasiswa/".$id_tl."'</script>";
			}
		}
	}

	public function skBimbingan(){
		$where1 = array (
			'id_dosen' => $this->session->userdata('id'));
		$where2 = array (
			'status_bim' => "Approved");
		$data['query'] = $this->model_dosen->join_where_skBimbingan($where1,$where2);

		$this->load->view('dsn_header');
		$this->load->view('dsn_skBimbingan',$data);
		$this->load->view('dsn_footer');
	}

	public function siapSidang($id){
		$table = "bimbingan";
		$where1 = array (
			'id_mahasiswa' => $id);
		$where2 = array (
			'id_dosen' => $this->session->userdata('id'));
		$data = array (
			'status_bim' => "Finished");
		$query = $this->model_dosen->update2($table,$data,$where1,$where2);

		if(isset($query)){
			echo "<script>alert('Berhasil merubah status mahasiswa menjadi siap sidang.');window.location.href='../skBimbingan'</script>";
		}else{
			echo "<script>alert('Gagal merubah status mahasiswa!');window.location.href='../skBimbingan'</script>";
		}
	}

	public function skSiapSidang(){
		$where1 = array (
			'id_dosen' => $this->session->userdata('id'));
		$where2 = array (
			'status_bim' => "Finished");
		$data['query'] = $this->model_dosen->join_where_skBimbingan($where1,$where2);

		$this->load->view('dsn_header');
		$this->load->view('dsn_skSiapSidang',$data);
		$this->load->view('dsn_footer');
	}

}