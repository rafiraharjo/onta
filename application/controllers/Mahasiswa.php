<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {

	public function __construct(){
		parent:: __construct();

		$this->load->model('model_mahasiswa');
		if($this->session->userdata('level') == '' || $this->session->userdata('level') != 'mahasiswa'){
			redirect("Login/");
		}
	}

	public function index()
	{
		$table = "bimbingan";
		$where = array (
			'id_mahasiswa' => $this->session->userdata('id'));
		$query['row'] = $this->model_mahasiswa->get_where_numRows($table,$where);

		if($query['row'] > 0){
			$bimb = $this->model_mahasiswa->join_where_request3($where);
			$data['con'] = array (
				'condition' => 1);
			$table2 = "tb_bimbingan";
			$where2 = array (
				'NIM' => $this->session->userdata('nim'));
			$data['query'] = $this->model_mahasiswa->get_where($table,$where);
		}else{
			$data['con'] = array (
				'condition' => 0);
		}

		$this->load->view('mhs_header');
		$this->load->view('mhs_dashboard',$data);
		$this->load->view('mhs_footer');
	}

	public function profil()
	{
		$table = "mahasiswa";
		$where = array (
			'mahasiswa.id' => $this->session->userdata('id'));
		$data['query'] = $this->model_mahasiswa->get_where($table,$where);

		$table2 = "fakultas";
		$data['query2'] = $this->model_mahasiswa->get_all($table2);

		$table3 = "jurusan";
		$data['query3'] = $this->model_mahasiswa->get_all($table3);

		$table4 = "kelas";
		$data['query4'] = $this->model_mahasiswa->get_all($table4);

		$table5 = "peminatan";
		$data['query5'] = $this->model_mahasiswa->get_all($table5);

		$where2 = array (
			'mahasiswa.id' => $this->session->userdata('id'));
		$data['query6'] = $this->model_mahasiswa->join_where_profil($where2); 
		
		$this->load->view('mhs_header');
		$this->load->view('mhs_profil', $data);
		$this->load->view('mhs_footer');
	}

	public function updateProfil(){
		if($this->input->post('submit')){
			$table = "mahasiswa";
			$where = array (
				'id' => $this->session->userdata('id'));
			if($this->input->post('password') != ''){
				$data = array (
					'NIM' => $this->input->post('nim'),
			        'nama_mhs' => $this->input->post('nama'),
			        'jk' => $this->input->post('jk'),
			        'email' => $this->input->post('email'),
			        'username' => $this->input->post('username'),
			        'password' => $this->input->post('password'),
			        'id_pmntn' => $this->input->post('peminatan'),
			        'id_kls' => $this->input->post('kelas'));
			}else{
				$data = array (
					'NIM' => $this->input->post('nim'),
			        'nama_mhs' => $this->input->post('nama'),
			        'jk' => $this->input->post('jk'),
			        'email' => $this->input->post('email'),
			        'username' => $this->input->post('username'),
			        'id_pmntn' => $this->input->post('peminatan'),
			        'id_kls' => $this->input->post('kelas'));
			}
		    $query = $this->model_mahasiswa->update($table,$data,$where);

		    if(isset($query)){
		        echo "<script>alert('Profil berhasil disimpan.');window.location.href='profil'</script>";
		    }else{
		        echo "<script>alert('Profil gagal disimpan!');window.location.href='profil'</script>";
		    }
		}
	}

	public function requestDosen()
	{
		$table = "bimbingan";
		$where = array (
			'id_mahasiswa' => $this->session->userdata('id'));
		$query['row'] = $this->model_mahasiswa->get_where_numRows($table,$where);

		if($query['row'] > 0){
			$bimb = $this->model_mahasiswa->join_where_request3($where);
			$stat_bim = $bimb[0]['status_bim'];
			$catatan = $bimb[0]['catatan'];
			$dsn = $bimb[0]['nama_dsn'];
			$data['con'] = array (
				'condition' => 1,
				'catatan' => $catatan,
				'status_bim' => $stat_bim,
				'nama_dsn' => $dsn);
		}else{
			$data['con'] = array (
				'condition' => 0);
		}

		$table2 = "mahasiswa";
		$where2 = array (
			'mahasiswa.id' => $this->session->userdata('id'));
		$data['query'] = $this->model_mahasiswa->get_where($table2,$where2);
		$data['query2'] = $this->model_mahasiswa->join_where_request($where2); 
		$data['query3'] = $this->model_mahasiswa->join_where_request2($where2);

		$this->load->view('mhs_header');
		$this->load->view('mhs_requestDosen',$data);
		$this->load->view('mhs_footer');
	}

	public function proses_requestDosen()
	{
		if($this->input->post('submit')){
			$table = "bimbingan";
			$where = array (
				'id_mahasiswa' => $this->session->userdata('id'));
			$query['row'] = $this->model_mahasiswa->get_where_numRows($table,$where);

			if($query['row'] == 0){
				$table1 = "dosen";
				$where1 = array (
					'id' => $this->input->post('dosen'));
				$data1 = $this->model_mahasiswa->get_where($table1,$where1);
				$kuota = $data1[0]['kuota'];

				if($kuota != 0){
					$table2 = "bimbingan";
					$data = array (
						'id_dosen' => $this->input->post('dosen'),
						'id_mahasiswa' => $this->session->userdata('id'),
						'catatan' => "",
						'tema' => $this->input->post('tema'),
						'file' => $this->input->post('abstraksi'),
						'status_bim' => "Submitted");
					$query1 = $this->model_mahasiswa->create($table2,$data);

					$data2 = array (
					'kuota' => $kuota-1);
					$query2 = $this->model_mahasiswa->update($table1,$data2,$where1);

					if(isset($query1) && isset($query2)){
						echo "<script>alert('Request dosen berhasil.');window.location.href='requestDosen'</script>";
					}else{
						echo "<script>alert('Request dosen gagal!');window.location.href='requestDosen'</script>";
					}
				}else{
					echo "<script>alert('Request dosen gagal! Kuota bimbingan dosen habis.');window.location.href='requestDosen'</script>";
				}
			}else{
				$table1 = "dosen";
				$where1 = array (
					'id' => $this->input->post('dosen'));
				$data1 = $this->model_mahasiswa->get_where($table1,$where1);
				$kuota = $data1[0]['kuota'];

				if($kuota != 0){
					$table2 = "bimbingan";
					$where2 = array (
						'id_mahasiswa' => $this->session->userdata('id')); 
					$data = array (
						'id_dosen' => $this->input->post('dosen'),
						'catatan' => "",
						'tema' => $this->input->post('tema'),
						'file' => $this->input->post('abstraksi'),
						'status_bim' => "Submitted");
					$query1 = $this->model_mahasiswa->update($table2,$data,$where2);

					$data2 = array (
					'kuota' => $kuota-1);
					$query2 = $this->model_mahasiswa->update($table1,$data2,$where1);

					if(isset($query1) && isset($query2)){
						echo "<script>alert('Request dosen berhasil.');window.location.href='requestDosen'</script>";
					}else{
						echo "<script>alert('Request dosen gagal!');window.location.href='requestDosen'</script>";
					}
				}else{
					echo "<script>alert('Request dosen gagal! Kuota bimbingan dosen habis.');window.location.href='requestDosen'</script>";
				}
			}
		}
	}

	public function timelineProgress()
	{
		$where = array (
			'bimbingan.id_mahasiswa' => $this->session->userdata('id'));
		$data['query'] = $this->model_mahasiswa->join_where_tema($where);

		$where2 = array (
			'mahasiswa.id' => $this->session->userdata('id'));
		$query['row'] = $this->model_mahasiswa->join_where_timeline_numRows($where2);

		if($query['row'] > 0){
			$data['query2'] = $this->model_mahasiswa->join_where_timeline($where2);
			$data['con'] = array (
				'condition' => 1);
		}else{
			$data['con'] = array (
				'condition' => 0);
		}

		$this->load->view('mhs_header');
		$this->load->view('mhs_timelineProgress',$data);
		$this->load->view('mhs_footer');
	}

	public function detailProgress($id)
	{
		$table = "progress";
		$where1 = array (
			'progress.id_timeline' => $id);
		$where2 = array (
			'progress.id_mahasiswa' => $this->session->userdata('id'));

		$query['row'] = $this->model_mahasiswa->get_andWhere_numRows($table,$where1,$where2);

		if($query['row'] > 0){
			$data['query'] = $this->model_mahasiswa->join_where_progress($where1,$where2);
			$data['con'] = array (
				'condition' => 1);
		}else{
			$table3 = "timeline";
			$where3 = array (
				'id' => $id);
			$data['query'] = $this->model_mahasiswa->get_where($table3,$where3);
			$data['con'] = array (
				'condition' => 0);
		}

		$this->load->view('mhs_header');
		$this->load->view('mhs_detailProgress',$data);
		$this->load->view('mhs_footer');
	}

	public function submitProgress($id){
		if($this->input->post('submit')){
			$table = "progress";
			$where1 = array (
				'id_mahasiswa' => $this->session->userdata('id'));
			$where2 = array (
				'id_timeline' => $id);
			$prog['row'] = $this->model_mahasiswa->get_andWhere_numRows($table,$where1,$where2);

			if($prog['row'] == 0){
				$data = array (
					'id_mahasiswa' => $this->session->userdata('id'),
					'id_timeline' => $id,
					'file' => $this->input->post('progress'),
					'status' => "Submitted");
				$query = $this->model_mahasiswa->create($table,$data);
			}else{
				$pr = $this->model_mahasiswa->get_andWhere($table,$where1,$where2);
				$id_pr = $pr[0]['id'];
				$where3 = array (
					'id' => $id_pr);
				$data = array (
					'file' => $this->input->post('progress'),
					'review' => "",
					'status' => "Submitted");
				$query = $this->model_mahasiswa->update($table,$data,$where3);
			}

			if(isset($query)){
				echo "<script>alert('Submit progress berhasil.');window.location.href='../detailProgress/".$id."'</script>";
			}else{
				echo "<script>alert('Submit progress gagal!');window.location.href='../detailProgress/".$id."'</script>";
			}
		}
	}

}