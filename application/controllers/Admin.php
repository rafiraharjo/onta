<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('model_admin');
		if($this->session->userdata('level') == '' || $this->session->userdata('level') != 'admin'){
			redirect("Login/");
		}
	}

	public function index()
	{
		$this->load->view('adm_header');
		$this->load->view('adm_dashboard');
		$this->load->view('adm_footer');
	}

	public function profil()
	{
		$table = "admin";
		$where = array (
			'id' => $this->session->userdata('id'));
		$data['profile'] = $this->model_admin->get_where($table,$where);
		// var_dump($data);
		$this->load->view('adm_header');
		$this->load->view('adm_profil',$data);
		$this->load->view('adm_footer');
	}

	public function editAdmin()
	{
		$table = "admin";
		$data = array (
			'NIK' => $this->input->post('nik'),
			'nama' => $this->input->post('nama'),
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password')
		);
		$where = array ('id' => $this->session->userdata('id'));
		$query = $this->model_admin->update($table,$data,$where);

		if(isset($query)){
			echo "<script>alert('Profil berhasil disimpan.');window.location.href='profil'</script>";
		}else{
			echo "<script>alert('Profil gagal disimpan!');window.location.href='profil'</script>";
		}
	}

	//Menu Dosen

	public function akunDosen()
	{
		$data['dosen'] = $this->model_admin->getDosen();				
		$this->load->view('adm_header');
		$this->load->view('adm_akunDosen',$data);
		$this->load->view('adm_footer');
	}

	public function tambahDosen()
	{
		$table1 = "fakultas";
		$data['fks'] = $this->model_admin->get_all($table1);
		$table2 = 'jurusan';
		$data['jur'] = $this->model_admin->get_all($table2);	
		$table3 = 'peminatan';
		$data['pmnts'] = $this->model_admin->get_all($table3);	
		//  var_dump($data);
		$this->load->view('adm_header');
		$this->load->view('adm_tambahDosen',$data);
		$this->load->view('adm_footer');
	}

	public function CreateDosen(){
		$table = "dosen";
		$data = array (
			'NIP' => $this->input->post('nip'),
			'nama_dsn' => $this->input->post('nama'),
			'jk' => $this->input->post('jk'),
			'email' => $this->input->post('email'),
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
			'id_pmntn' => $this->input->post('peminatan'),
			'kuota' => $this->input->post('kuota'));
		$query = $this->model_admin->create($table,$data);

		if(isset($query)){
			echo "<script>alert('Dosen berhasil ditambahkan.');window.location.href='akunDosen'</script>";
		}else{
			echo "<script>alert('Dosen gagal ditambahkan!');window.location.href='akunDosen'</script>";
		}
	}

	public function hapusDosen($id){
		$table = "dosen";
		$where = array('id' => $id);
		$query = $this->model_admin->delete($table,$where);

		if(isset($query)){
			echo "<script>alert('Dosen berhasil dihapus.');window.location.href='../akunDosen'</script>";
		}else{
			echo "<script>alert('Dosen gagal dihapus!');window.location.href='../akunDosen'</script>";
		}
	}

	//Menu Mahasiswa

	public function akunMahasiswa()
	{
		$data['mhs'] = $this->model_admin->getMahasiswa();
		$this->load->view('adm_header');
		$this->load->view('adm_akunMahasiswa',$data);
		$this->load->view('adm_footer');
	}

	public function hapusMahasiswa($id){
		$table = "mahasiswa";
		$where = array('id' => $id);
		$query = $this->model_admin->delete($table,$where);

		if(isset($query)){
			echo "<script>alert('Mahasiswa berhasil dihapus.');window.location.href='../akunMahasiswa'</script>";
		}else{
			echo "<script>alert('Mahasiswa gagal dihapus!');window.location.href='../akunMahasiswa'</script>";
		}
	}

	//Menu Fakultas

	public function fakultas()
	{
		$table = "fakultas";
		$data['fks'] = $this->model_admin->get_all($table);	
		$this->load->view('adm_header');
		$this->load->view('adm_fakultas',$data);
		$this->load->view('adm_footer');
	}

	public function tambahFakultas()
	{
		$this->load->view('adm_header');
		$this->load->view('adm_tambahFakultas');
		$this->load->view('adm_footer');
	}

	public function CreateFakultas(){
		$table = "fakultas";
		$data = array (
			'nama_fks' => $this->input->post('fakultas'),
			'kode_fks' => $this->input->post('kode')
		);
		$query = $this->model_admin->create($table,$data);

		if(isset($query)){
			echo "<script>alert('Fakultas berhasil ditambahkan.');window.location.href='fakultas'</script>";
		}else{
			echo "<script>alert('Fakultas gagal ditambahkan!');window.location.href='fakultas'</script>";
		}
	}

	public function editFakultas($id)
	{
		$table = "fakultas";
		$where = array ('id' => $id);
		$data['fks'] = $this->model_admin->get_where($table, $where);

		$this->load->view('adm_header');
		$this->load->view('adm_editFakultas', $data);
		$this->load->view('adm_footer');
	}

	public function updateFk($id){			
		$table = "fakultas";
		$data = array (
			'nama_fks' => $this->input->post('fakultas'),
			'kode_fks' => $this->input->post('kode')
		);
		$where = array ('id' => $id);
		$query = $this->model_admin->update($table,$data,$where);

		if(isset($query)){
			echo "<script>alert('Fakultas berhasil diubah.');window.location.href='../fakultas'</script>";
		}else{
			echo "<script>alert('Fakultas gagal diubah!');window.location.href='../fakultas'</script>";
		}
	}

	public function hapusFakultas($id){
		$table = "fakultas";
		$where = array('id' => $id);
		$query = $this->model_admin->delete($table,$where);

		if(isset($query)){
			echo "<script>alert('Fakultas berhasil dihapus.');window.location.href='../fakultas'</script>";
		}else{
			echo "<script>alert('Fakultas gagal dihapus!');window.location.href='../fakultas'</script>";
		}
	}

	//Menu Jurusan

	public function jurusan()
	{
		$data['sjr'] = $this->model_admin->getJurusan();
		$this->load->view('adm_header');
		$this->load->view('adm_jurusan',$data);
		$this->load->view('adm_footer');
	}

	public function tambahJurusan()
	{
		$table = "fakultas";
		$data['fks'] = $this->model_admin->get_all($table);
		$this->load->view('adm_header');
		$this->load->view('adm_tambahJurusan',$data);
		$this->load->view('adm_footer');
	}

	public function CreateJurusan(){
		$table = "jurusan";
		$data = array (
			'nama_jur' => $this->input->post('jurusan'),
			'id_fks' => $this->input->post('fakultas')
		);	
		$query = $this->model_admin->create($table,$data);

		if(isset($query)){
			echo "<script>alert('Jurusan berhasil ditambahkan.');window.location.href='jurusan'</script>";
		}else{
			echo "<script>alert('Jurusan gagal ditambahkan!');window.location.href='jurusan'</script>";
		}
	}

	public function editJurusan($id)
	{
		$where = array ('jurusan.id' => $id);
		$data['sjur'] = $this->model_admin->get_where_jurusan($where);
		$table = "fakultas";
		$data['fks'] = $this->model_admin->get_all($table);

		$this->load->view('adm_header');
		$this->load->view('adm_editJurusan',$data);
		$this->load->view('adm_footer');
	}

	public function updateJurusan($id){			
		$table = "jurusan";
		$data = array (
			'nama_jur' => $this->input->post('jurusan'),
			'id_fks' => $this->input->post('fakultas')
		);
		$where = array ('id' => $id);		
		$query = $this->model_admin->update($table,$data,$where);

		if(isset($query)){
			echo "<script>alert('Jurusan berhasil diubah.');window.location.href='../jurusan'</script>";
		}else{
			echo "<script>alert('Jurusan gagal diubah!');window.location.href='../jurusan'</script>";
		}
	}

	public function hapusJurusan($id){
		$table = "jurusan";
		$where = array('id' => $id);
		$query = $this->model_admin->delete($table,$where);

		if(isset($query)){
			echo "<script>alert('Jurusan berhasil dihapus.');window.location.href='../jurusan'</script>";
		}else{
			echo "<script>alert('Jurusan gagal dihapus!');window.location.href='../jurusan'</script>";
		}
	}

	//Menu Kelas

	public function kelas()
	{
		$data['kls'] = $this->model_admin->getKelas();

		$this->load->view('adm_header');
		$this->load->view('adm_kelas',$data);
		$this->load->view('adm_footer');
	}

	public function tambahKelas()
	{
		$table = "jurusan";
		$data['sjr'] = $this->model_admin->get_all($table);

		$this->load->view('adm_header');
		$this->load->view('adm_tambahKelas',$data);
		$this->load->view('adm_footer');
	}

	public function CreateKelas(){
		$table = "kelas";
		$data = array (
			'nama_kls' => $this->input->post('kelas'),
			'id_jur' => $this->input->post('jurusan')
		);
		$query = $this->model_admin->create($table,$data);

		if(isset($query)){
			echo "<script>alert('Kelas berhasil ditambahkan.');window.location.href='kelas'</script>";
		}else{
			echo "<script>alert('Kelas gagal ditambahkan!');window.location.href='kelas'</script>";
		}
	}

	public function editKelas($id)
	{
		$where = array ('kelas.id' => $id);
		$data['kls'] = $this->model_admin->get_where_kelas($where);
		$table = "jurusan";
		$data['sjr'] = $this->model_admin->get_all($table);

		$this->load->view('adm_header');
		$this->load->view('adm_editKelas',$data);
		$this->load->view('adm_footer');
	}

	public function UpdateKelas($id){	
		$table = "kelas";
		$data = array (
			'nama_kls' => $this->input->post('kelas'),
			'id_jur' => $this->input->post('jurusan')
		);
		$where = array ('id' => $id);
		$query = $this->model_admin->update($table,$data,$where);

		if(isset($query)){
			echo "<script>alert('Kelas berhasil diubah.');window.location.href='../kelas'</script>";
		}else{
			echo "<script>alert('Kelas gagal diubah!');window.location.href='../kelas'</script>";
		}
	}

	public function hapusKelas($id){
		$table = "kelas";
		$where = array('id' => $id);
		$query = $this->model_admin->delete($table,$where);

		if(isset($query)){
			echo "<script>alert('Kelas berhasil dihapus.');window.location.href='../kelas'</script>";
		}else{
			echo "<script>alert('Kelas gagal dihapus!');window.location.href='../kelas'</script>";
		}
	}	

	//Menu Peminatan

	public function peminatan()
	{
		$data['pmnt'] = $this->model_admin->getPeminatan();

		$this->load->view('adm_header');
		$this->load->view('adm_peminatan',$data);
		$this->load->view('adm_footer');
	}

	public function tambahPeminatan()
	{
		$table = "jurusan";
		$data['sjr'] = $this->model_admin->get_all($table);

		$this->load->view('adm_header');
		$this->load->view('adm_tambahPeminatan',$data);
		$this->load->view('adm_footer');
	}

	public function CreatePeminatan()
	{
		$table = "peminatan";
		$data = array (
			'nama_pmntn' => $this->input->post('peminatan'),
			'id_jur' => $this->input->post('jurusan')
		);
		$query = $this->model_admin->create($table,$data);

		if(isset($query)){
			echo "<script>alert('Peminatan berhasil ditambahkan.');window.location.href='peminatan'</script>";
		}else{
			echo "<script>alert('Peminatan gagal ditambahkan!');window.location.href='peminatan'</script>";
		}
	}

	public function editPeminatan($id)
	{
		$where = array ('peminatan.id' => $id);
		$data['pmntn'] = $this->model_admin->get_where_peminatan($where);
		$table = "jurusan";
		$data['sjr'] = $this->model_admin->get_all($table);

		$this->load->view('adm_header');
		$this->load->view('adm_editPeminatan',$data);
		$this->load->view('adm_footer');
	}

	public function UpdatePeminatan($id){			
		$table = "peminatan";
		$data = array (
			'nama_pmntn' => $this->input->post('peminatan'),
			'id_jur' => $this->input->post('jurusan')
		);
		$where = array ('id' => $id);
		$query = $this->model_admin->update($table,$data,$where);

		if(isset($query)){
			echo "<script>alert('Peminatan berhasil diubah.');window.location.href='../peminatan'</script>";
		}else{
			echo "<script>alert('Peminatan gagal diubah!');window.location.href='../peminatan'</script>";
		}
	}

	public function hapusPeminatan($id){
		$table = "peminatan";
		$where = array('id' => $id);
		$query = $this->model_admin->delete($table,$where);

		if(isset($query)){
			echo "<script>alert('Peminatan berhasil dihapus.');window.location.href='../peminatan'</script>";
		}else{
			echo "<script>alert('Peminatan gagal dihapus!');window.location.href='../peminatan'</script>";
		}
	}	

	//Menu SK Bimbingan

	public function skBimbingan(){
		$data['query'] = $this->model_admin->getDosen();

		$this->load->view('adm_header');
		$this->load->view('adm_skBimbingan',$data);
		$this->load->view('adm_footer');
	}

	public function mhsBimbingan($id){
		$where1 = array (
			'bimbingan.id_dosen' => $id);
		$where2 = array (
			'status_bim' => "Approved");
		$data['query'] = $this->model_admin->join_where_sk($where1,$where2);

		$this->load->view('adm_header');
		$this->load->view('adm_listBimbingan',$data);
		$this->load->view('adm_footer');
	}

	//Menu SK Siap Sidang

	public function skSiapSidang(){
		$data['query'] = $this->model_admin->getDosen();

		$this->load->view('adm_header');
		$this->load->view('adm_skSiapSidang',$data);
		$this->load->view('adm_footer');
	}

	public function mhsSiapSidang($id){
		$where1 = array (
			'bimbingan.id_dosen' => $id);
		$where2 = array (
			'status_bim' => "Finished");
		$data['query'] = $this->model_admin->join_where_sk($where1,$where2);

		$this->load->view('adm_header');
		$this->load->view('adm_listSiapSidang',$data);
		$this->load->view('adm_footer');
	}			
}
?>