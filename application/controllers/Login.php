<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		if($this->session->userdata('level') == "mahasiswa"){
			redirect("Mahasiswa/");
		}elseif($this->session->userdata('level') == "dosen"){
			redirect("Dosen/");
		}elseif($this->session->userdata('level') == "admin"){
			redirect("Admin/");
		}else{
			$this->load->view('login');
		}
	}

	public function mahasiswa()
	{
		if($this->session->userdata('level') == "mahasiswa"){
			redirect("Mahasiswa/");
		}elseif($this->session->userdata('level') == "dosen"){
			redirect("Dosen/");
		}elseif($this->session->userdata('level') == "admin"){
			redirect("Admin/");
		}else{
			$this->load->view('loginMahasiswa');
		}
	}

	public function register()
	{
		if($this->session->userdata('level') == "mahasiswa"){
			redirect("Mahasiswa/");
		}elseif($this->session->userdata('level') == "dosen"){
			redirect("Dosen/");
		}elseif($this->session->userdata('level') == "admin"){
			redirect("Admin/");
		}else{
			$this->load->view('registerMahasiswa');
		}
	}

	public function registerMahasiswa()
	{
		$this->load->model('model_login');
		if($this->input->post('submit')){
			$nim = $this->input->post('nim');
			$table1 = "import_nim";
			$where = array (
				'NIM' => $nim);
			$query1['data'] = $this->model_login->get_where_numRows($table1,$where);

			if(($query1['data']) > 0){
				$table2 = "mahasiswa";
				$query2['data'] = $this->model_login->get_where_numRows($table2,$where);
				if(($query2['data']) == 0){
					echo "<script>alert('NIM ditemukan, silahkan melanjutkan proses registrasi.');window.location.href='register2/$nim'</script>";
				}else{
					echo "<script>alert('NIM telah dipakai!');window.location.href='register'</script>";
				}
			}else{
				echo "<script>alert('NIM tidak ditemukan!');window.location.href='register'</script>";
	        }
		}
	}

	public function register2($nim)
	{
		if($this->session->userdata('level') == "mahasiswa"){
			redirect("Mahasiswa/");
		}elseif($this->session->userdata('level') == "dosen"){
			redirect("Dosen/");
		}elseif($this->session->userdata('level') == "admin"){
			redirect("Admin/");
		}else{
			$data['nim'] = $nim;
			$this->load->view('registerMahasiswa2', $data);
		}
	}

	public function registerMahasiswa2($nim)
	{
		$this->load->model('model_login');
		if($this->input->post('submit')){
			$table = "mahasiswa";
			$where1 = array (
				'username' => $this->input->post('username'));
			$query['data'] = $this->model_login->get_where_numRows($table,$where1);

			if(($query['data']) > 0){
				echo "<script>alert('Registrasi gagal! Username telah dipakai.');window.location.href='../register'</script>";
			}else{
				$data_add = array (
	                'NIM' => $nim,
	                'nama_mhs' => $this->input->post('nama'),
	                'jk' => $this->input->post('jk'),
	                'email' => $this->input->post('email'),
	                'username' => $this->input->post('username'),
	               	'password' => $this->input->post('password'));
	            $query = $this->model_login->create($table,$data_add);

	            if(isset($query)){
	            	echo "<script>alert('Registrasi Berhasil. Silahkan login!');window.location.href='../mahasiswa'</script>";
	            }else{
	            	echo "<script>alert('Registrasi gagal!');window.location.href='../register'</script>";
	            }
	        }
		}
	}

	public function dosen()
	{
		if($this->session->userdata('level') == "mahasiswa"){
			redirect("Mahasiswa/");
		}elseif($this->session->userdata('level') == "dosen"){
			redirect("Dosen/");
		}elseif($this->session->userdata('level') == "admin"){
			redirect("Admin/");
		}else{
			$this->load->view('loginDosen');
		}
	}

	public function admin()
	{
		if($this->session->userdata('level') == "mahasiswa"){
			redirect("Mahasiswa/");
		}elseif($this->session->userdata('level') == "dosen"){
			redirect("Dosen/");
		}elseif($this->session->userdata('level') == "admin"){
			redirect("Admin/");
		}else{
			$this->load->view('loginAdmin');
		}
	}

	public function loginMahasiswa()
	{
		$this->load->model('model_login');
		if($this->input->post('submit')){
			$table = "mahasiswa";
			$user = $this->input->post('username');
			$pass = $this->input->post('password');

			$data = array (
				'username' => $user,
				'password' => $pass);
			$query['data'] = $this->model_login->get_where_numRows($table,$data);

			if(($query['data']) > 0){
				$query1 = $this->model_login->get_where($table,$data);
				$id = $query1[0]['id'];
				$nim = $query1[0]['NIM'];
				$nama = $query1[0]['nama_mhs'];

				$data_session = array(
				'id' => $id,
				'nim' => $nim,
				'nama' => $nama,
				'level' => "mahasiswa");

				$this->session->set_userdata($data_session);
				redirect("Mahasiswa/");
			}else{
	            echo "<script>alert('Login gagal! Username atau password salah.');window.location.href='mahasiswa'</script>";
	        }
		}
	}

	public function loginDosen()
	{
		$this->load->model('model_login');
		if($this->input->post('submit')){
			$table = "dosen";
			$user = $this->input->post('username');
			$pass = $this->input->post('password');

			$data = array (
				'username' => $user,
				'password' => $pass);
			$query['data'] = $this->model_login->get_where_numRows($table,$data);

			if(($query['data']) > 0){
				$query1 = $this->model_login->get_where($table,$data);
				$id = $query1[0]['id'];
				$nip = $query1[0]['NIP'];
				$nama = $query1[0]['nama_dsn'];

				$data_session = array(
				'id' => $id,
				'nip' => $nip,
				'nama' => $nama,
				'level' => "dosen");

				$this->session->set_userdata($data_session);
				redirect("Dosen/");
			}else{
	            echo "<script>alert('Login gagal! Username atau password salah.');window.location.href='dosen'</script>";
	        }
		}
	}

	public function loginAdmin()
	{
		$this->load->model('model_login');
		if($this->input->post('submit')){
			$table = "admin";
			$user = $this->input->post('username');
			$pass = $this->input->post('password');

			$data = array (
				'username' => $user,
				'password' => $pass);
			$query['data'] = $this->model_login->get_where_numRows($table,$data);

			if(($query['data']) > 0){
				$query1 = $this->model_login->get_where($table,$data);
				$id = $query1[0]['id'];
				$nik = $query1[0]['NIK'];
				$nama = $query1[0]['nama'];

				$data_session = array(
				'id' => $id,
				'nip' => $nik,
				'nama' => $nama,
				'level' => "admin");

				$this->session->set_userdata($data_session);
				redirect("Admin/");
			}else{
	            echo "<script>alert('Login gagal! Username atau password salah.');window.location.href='admin'</script>";
	        }
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('Login/');
	}
}